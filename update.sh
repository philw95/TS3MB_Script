#!/bin/bash

FILEPATH=$(readlink -f "$0")
BINARYPATH="$(dirname "${FILEPATH}")"

# Define colors for printf
MAIN='\033[0m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
FLASH='\033[5m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

# Zeichen
GOOD="[${GREEN}✓${MAIN}]"
ERROR="[${RED}✗${MAIN}]"
NEUTRAL="[${YELLOW}-${MAIN}]"
INFO="[i]"

# function
check_exit () {
if ! [ "$?" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: ${1} Exit state: ${2} \n"
	exit 1
fi
}

good_edit () {
printf "\033[s\033[1A\r\033[1C${GOOD}\033[u"
}

error_edit () {
printf "\033[s\033[1A\r\033[1C${ERROR}\033[u"
}

bold () {
printf "${BOLD}${1}${MAIN}"
}

spinner () {
while true
do
  printf "\033[s\033[1A\r\033[2C${YELLOW}\\\\${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}|${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}/${MAIN}\033[u" ; sleep 0.15
  printf "\033[s\033[1A\r\033[2C${YELLOW}-${MAIN}\033[u" ; sleep 0.15
done
}

spinner_background () {

PID="$!"
if [ "${NO_OUTPUT}" = "1" ]; then
	${1} &> /dev/null
else
	${1}
fi
EXITSTATE="$?"
if ! [ "${EXITSTATE}" = "0" ]; then
	error_edit
	echo
	printf " ${ERROR} ${RED}ERROR!${MAIN} Aborted. | Flaw: '${2}' Exit state: ${EXITSTATE} \n"
	kill $PID
	wait $PID 2> /dev/null
	exit 1
fi
good_edit
kill $PID
wait $PID 2> /dev/null

}
# Example for spinner:
# spinner & spinner_background "apt-get update" "apt-get update"


# Export functions and Variables
export -f check_exit good_edit error_edit bold spinner spinner_background
export MAIN BOLD UNDERLINE FLASH RED GREEN ORANGE BLUE PURPLE CYAN LIGHTGRAY DARKGRAY LIGHTRED LIGHTGREEN YELLOW LIGHTBLUE LIGHTPURPLE LIGHTCYAN WHITE GOOD ERROR NEUTRAL INFO

# Banner
clear
printf "${BOLD}${LIGHTGREEN}  ________________ __  ___           _      ____        __ ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} /_  __/ ___/__  //  |/  /_  _______(_)____/ __ )____  / /_${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  / /  \__ \ /_ </ /|_/ / / / / ___/ / ___/ __  / __ \/ __/${MAIN}\n"
printf "${BOLD}${LIGHTGREEN} / /  ___/ /__/ / /  / / /_/ (__  ) / /__/ /_/ / /_/ / /_  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}/_/  /____/____/_/  /_/\__,_/____/_/\___/_____/\____/\__/  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}                                                           ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}          ______            __             __${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}         / ____/___  ____  / /__________  / /${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}        / /   / __ \/ __ \/ __/ ___/ __ \/ / ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       / /___/ /_/ / / / / /_/ /  / /_/ / /  ${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}       \____/\____/_/ /_/\__/_/   \____/_/ ${MAIN}\n\n"

echo
if [ "$(id -u)" = "0" ]; then
        printf " ${GOOD} Root User \n"
else
        printf " ${ERROR} Not Root User! You must have Root Rights! Use 'su' or 'sudo -i' \n"
        exit 1
fi

if ! [ -f settings.conf ] || ! [ -f bots.ini ]; then
	printf " ${ERROR} Can't find data! Please make sure that you are in the Folder of the Script.\n     with ls you must see this files: settings.conf, bots.ini, ... \n"
	exit 1
fi

USERNAME="TS3MusicBot_Control"
egrep "^${USERNAME}" /etc/passwd > /dev/null
if [ $? -eq 0 ]; then
	printf " ${GOOD} User ${USERNAME} Exist. \n"
	if [ -d /home/${USERNAME} ]; then
		printf " ${GOOD} Home directory Exist. \n"
	else
		printf " ${INFO} Home directory don't Exist! (Creating now) \n"
		mkdir /home/${USERNAME} ; check_exit "mkdir" "$?"
		chown ${USERNAME}:users /home/${USERNAME} ; check_exit "chown" "$?"
		good_edit
	fi
else
	printf " ${INFO} User ${USERNAME} Dosen't Exist! (Creating Now) \n"
	useradd -g users -d /home/TS3MusicBot_Control -m -s /bin/bash TS3MusicBot_Control 2> /dev/null ; check_exit "useradd" "$?"
	passwd -d TS3MusicBot_Control &> /dev/null ; check_exit "passwd" "$?"
	good_edit
fi

LINUX_VERSION=$(cat /etc/issue)
LINUX_VERSION2=$(cat /etc/*-release)

# Check wich OS is Installed
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'Debian')
if [ -n "$VARIABLE_OS" ]; then
	OS="Debian"
	OS_VERSION=$(cat /etc/debian_version | awk -F. '{print $1}')
	if [ "$OS_VERSION" = "10" ] || [ "$OS_VERSION" = "9" ] || [ "$OS_VERSION" = "8" ]; then
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your Debian Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'Ubuntu')
if [ -n "$VARIABLE_OS" ]; then
	OS="Ubuntu"
	OS_VERSION=$(cat /etc/lsb-release | grep -i DISTRIB_RELEASE | grep -o '.....$')
	if [ "$OS_VERSION" = "14.04" ] || [ "$OS_VERSION" = "16.04" ] || [ "$OS_VERSION" = "18.04" ]; then
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your Ubuntu Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi
VARIABLE_OS=$(echo $LINUX_VERSION | grep -i 'CentOS')
VARIABLE_OS2=$(echo $LINUX_VERSION2 | grep -i 'CentOS')
if [ -n "$VARIABLE_OS" ] || [ -n "$VARIABLE_OS2" ]; then
	OS="CentOS"
	OS_VERSION=$(cat /etc/redhat-release | awk '{print $3}' | grep -o '^.')
	OS_VERSION2=$(cat /etc/redhat-release | awk '{print $4}' | grep -o '^.')
	if [ "$OS_VERSION" = "7" ]; then
		SUPPORTED_OS="OK"
	elif [ "$OS_VERSION2" = "7" ]; then
		OS_VERSION="$OS_VERSION2"
		SUPPORTED_OS="OK"
	else
		printf " ${ERROR} This Script don't support your CentOS Version! Detected OS: $OS $OS_VERSION \n"
	fi
fi

if ! [ "$SUPPORTED_OS" = "OK" ]; then
	printf " ${ERROR} ERROR! Check OS Error! Detected OS: $OS $OS_VERSION \n"
	echo
	printf " Please make sure you have Install this Packages:\n\n"
	printf "APT-GET: openjdk-X-jre 'X=7-11' wget whiptail screen dnsutils\n"
	printf "YUM    : java-1.X.0-openjdk 'X=7-11' wget newt screen bind-utils\n"
	echo
	printf " [Press any key to continue]"
	read -n 1 -s -r
	echo
else
	printf " ${INFO} Detected OS: $OS $OS_VERSION \n"
	sleep 2
fi

# Package installer
PACKETCOUNTER=0
JAVAINSTALLED=""
JAVAVERSION=""
if [ "$OS" = "Debian" ] || [ "$OS" = "Ubuntu" ]; then
	# Update System
	echo
	bold " ${INFO} Update your System \n"

	NO_OUTPUT="1"
	printf " ${INFO} Step 1: pre-configuring packages \n"
	spinner & spinner_background "dpkg --configure -a" "dpkg --configure -a"

	printf " ${INFO} Step 2: Fix and attempt to correct a system with broken dependencies \n"
	spinner & spinner_background "apt-get install -f" "apt-get install -f"

	printf " ${INFO} Step 3: Update apt cache \n"
	spinner & spinner_background "apt-get update" "apt-get update"

	printf " ${INFO} Step 4: Upgrade packages \n"
	spinner & spinner_background "apt-get upgrade -y" "apt-get upgrade"

	printf " ${INFO} Step 5: Distribution upgrade \n"
	spinner & spinner_background "apt-get dist-upgrade -y" "apt-get dist-upgrade"

	printf " ${INFO} Step 6: Remove unused packages \n"
	spinner & spinner_background "apt-get --purge autoremove -y" "apt-get --purge autoremove"

	printf " ${INFO} Step 7: Clean up \n"
	spinner & spinner_background "apt-get autoclean -y" "apt-get autoclean"
	NO_OUTPUT=""

	echo

	bold " ${INFO} Check required dependencies \n"
	# Java
	PACKETLISTJAVA=( openjdk-11-jre openjdk-10-jre openjdk-9-jre openjdk-8-jre openjdk-7-jre )
	for PACKET in "${PACKETLISTJAVA[@]}"
	do
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
			printf " ${GOOD} $PACKET \n"
			JAVAINSTALLED="1"
		fi
	done

	if [ ! "$JAVAINSTALLED" = "1" ]; then
		if apt-cache search openjdk | grep -qo "openjdk-[0-9]-jre " || apt-cache search openjdk | grep -qo "openjdk-[0-9][0-9]-jre "; then
			for PACKET in "${PACKETLISTJAVA[@]}"
			do
				if apt-cache search openjdk | grep -qo "$PACKET "; then
					JAVAVERSION="${PACKET}"
					break
				fi
			done
		fi
	fi

	# Packete prüfen, wenn nicht vorhanden installieren.
	PACKETLIST=( wget whiptail screen dnsutils lsof sqlite3 libsqlite3-dev $JAVAVERSION )
	for PACKET in "${PACKETLIST[@]}"
	do
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
			printf " ${GOOD} $PACKET \n"
		else
			printf " ${ERROR} $PACKET installing \n"
			NO_OUTPUT="1"
			spinner & spinner_background "apt-get install -y $PACKET" "apt-get install"
			NO_OUTPUT=""
			sleep 1
		fi
	done

	sleep 2
elif [ "$OS" = "CentOS" ]; then
	# Update System
	printf " ${INFO} Update your System: \n"

	NO_OUTPUT="1"
	printf " ${INFO} Step 1: Cleanup \n"
	spinner & spinner_background "yum clean all" "yum clean all"

	printf " ${INFO} Step 2: Update Packages \n"
	spinner & spinner_background "yum -q -y update" "yum update"

	printf " ${INFO} Step 3: Upgrade Packages \n"
	spinner & spinner_background "yum -q -y upgrade" "yum upgrade"

	printf " ${INFO} Step 4: Remove unused packages \n"
	spinner & spinner_background "yum -q -y autoremove" "yum autoremove"

	printf " ${INFO} Step 5: Cleanup \n"
	spinner & spinner_background "yum clean all" "yum clean all"
	NO_OUTPUT=""

	echo

	# Java
	PACKETLISTJAVA=( java-11-openjdk java-1.8.0-openjdk java-1.7.0-openjdk )
	for PACKET in "${PACKETLISTJAVA[@]}"
	do
		if rpm -q $PACKET &> /dev/null; then
			printf " ${GOOD} $PACKET installed. \n"
			JAVAINSTALLED="1"
		fi
	done

	if [ ! "$JAVAINSTALLED" = "1" ]; then
		for PACKET in "${PACKETLISTJAVA[@]}"
		do
			if yum list java-* | grep -qm 1 "$PACKET"; then
				JAVAVERSION="${PACKET}"
				break
			fi
		done
	fi

	PACKETLIST=( wget newt screen bind-utils lsof sqlite sqlite-devel $JAVAVERSION )
	for PACKET in "${PACKETLIST[@]}"
	do
		if rpm -q $PACKET &> /dev/null; then
			printf " ${GOOD} $PACKET installed \n"
		else
			printf " ${ERROR} $PACKET installing \n"
			NO_OUTPUT="1"
			spinner & spinner_background "yum install -y $PACKET" "yum install"
			NO_OUTPUT=""
			sleep 1
		fi
	done

	sleep 2
fi

echo

cd /home/${USERNAME}
if [ -f TS3MusicBot_Control.sh ]; then
	printf " ${GOOD} TS3MusicBot_Control.sh Already Exist! \n"
else
	bold " ${INFO} Download TS3MusicBot_Control \n"

	NO_OUTPUT="1"
	printf " ${INFO} Download TS3MusicBot_Control.tar \n"
	spinner & spinner_background "wget -T 2 -t 5 https://gitlab.com/philw95/ts3musicbot_control/raw/release_master/TS3MusicBot_Control.tar" "wget"

	printf " ${INFO} Extracting TS3MusicBot_Control.tar \n"
	spinner & spinner_background "tar xfv TS3MusicBot_Control.tar" "tar"

	printf " ${INFO} Delete TS3MusicBot_Control.tar \n"
	spinner & spinner_background "rm TS3MusicBot_Control.tar" "rm"

	printf " ${INFO} CHOWN files to ${USERNAME} \n"
	spinner & spinner_background "chown -R ${USERNAME} /home/TS3MusicBot_Control" "chown"
	NO_OUTPUT=""

	echo
fi

if [ -f /tmp/TS3MusicBot_Notportable_Mode ]; then touch /home/${USERNAME}/system/notportable; chown ${USERNAME}:users /home/${USERNAME}/system/notportable; rm /tmp/TS3MusicBot_Notportable_Mode; fi

printf " ${INFO} Create Link. (/usr/local/bin/TS3MusicBot_Control) \n"
NO_OUTPUT="1"
spinner & spinner_background "cp /home/${USERNAME}/TS3MusicBot_Control /usr/local/bin/" "cp"
NO_OUTPUT=""
echo
if ! [ -d /home/${USERNAME}/system/bot_files ]; then mkdir /home/${USERNAME}/system/bot_files; fi

echo "#####"
cd "${BINARYPATH}"

echo
bold " ${INFO} Migrate existing datas. ! Do not cancel ! \n\n"

source settings.conf

BOTA=($(< bots.ini ))
BOTS=${#BOTA[@]}
(( BOTS -=1 ))

DB_VERSION="1.0.0"

if [ "${MUSICANDRADIOFOLDER}" = "1" ]; then
	MUSICANDRADIOFOLDER="together"
else
	MUSICANDRADIOFOLDER="single"
fi

if [ "${WEBIFCHECK}" = "1" ]; then
	WEBIFCHECK="true"
else
	WEBIFCHECK="false"
fi

printf " ${INFO} Create SQlite Database with table settings. \n"; sqlite3 /home/${USERNAME}/system/settings.db  <<END_SQL
CREATE TABLE settings ( post_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , DB_Version TEXT NOT NULL , Language TEXT NOT NULL , Music_Radio_Folder TEXT NOT NULL , 
Webinterface_Check TEXT NOT NULL , Boot_Time TEXT NOT NULL , Restart_Time TEXT NOT NULL );
END_SQL
printf " ${INFO} Insert Settings into SQlite table settings. \n"; sqlite3 /home/${USERNAME}/system/settings.db  <<END_SQL
INSERT INTO settings ( DB_Version , Language , Music_Radio_Folder , Webinterface_Check , Boot_Time , Restart_Time ) VALUES ( '${DB_VERSION}' , '${LANGUAGE}' , 
'${MUSICANDRADIOFOLDER}' , '${WEBIFCHECK}' , '${BOOTTIME}' , '${RESTARTTIME}' );
END_SQL

if [ "$MUSICANDRADIOFOLDER" = "together" ]; then
	NO_OUTPUT="1"
	printf " ${INFO} Copy folder music. \n"
	spinner & spinner_background "cp -R music /home/${USERNAME}/music" "cp -R"

	printf " ${INFO} Copy folder radio \n"
	spinner & spinner_background "cp -R radio /home/${USERNAME}/radio" "cp -R"

	printf " ${INFO} Copy folder playlist \n"
	spinner & spinner_background "cp -R playlist /home/${USERNAME}/playlist" "cp -R"
	NO_OUTPUT=""
else
	NO_OUTPUT="1"
	printf " ${INFO} Create folder music if not Exist. \n"
	spinner & spinner_background "mkdir /home/${USERNAME}/music" "mkdir"

	printf " ${INFO} Create folder radio if not Exist. \n"
	spinner & spinner_background "mkdir /home/${USERNAME}/radio" "mkdir"

	printf " ${INFO} Create folder playlist if not Exist. \n"
	spinner & spinner_background "mkdir /home/${USERNAME}/playlist" "mkdir"
	NO_OUTPUT=""
fi

echo

BOTC=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}

	bold " ${INFO} Bot ${BOTC2}. \n"

	NO_OUTPUT="1"
	printf " ${INFO} Move Botfolder. \n"
	spinner & spinner_background "cp -R TS3MusicBot_${BOTC2} /home/${USERNAME}/system/bot_files/" "cp -R"
	NO_OUTPUT=""

	if ! [ "$MUSICANDRADIOFOLDER" = "together" ]; then
		if ! [ -d /home/${USERNAME}/music/TS3MusicBot_${BOTC2} ]; then mkdir /home/${USERNAME}/music/TS3MusicBot_${BOTC2}; fi
		if ! [ -d /home/${USERNAME}/radio/TS3MusicBot_${BOTC2} ]; then mkdir /home/${USERNAME}/radio/TS3MusicBot_${BOTC2}; fi
		if ! [ -d /home/${USERNAME}/playlist/TS3MusicBot_${BOTC2} ]; then mkdir /home/${USERNAME}/playlist/TS3MusicBot_${BOTC2}; fi

		NO_OUTPUT="1"
		printf " ${INFO} Copy folder music. \n"
		spinner & spinner_background "cp -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/music/. /home/${USERNAME}/music/TS3MusicBot_${BOTC2}/" "cp -R"

		printf " ${INFO} Copy folder radio. \n"
		spinner & spinner_background "cp -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/radio/. /home/${USERNAME}/radio/TS3MusicBot_${BOTC2}/" "cp -R"

		printf " ${INFO} Copy folder playlist. \n"
		spinner & spinner_background "cp -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/playlist/. /home/${USERNAME}/playlist/TS3MusicBot_${BOTC2}/" "cp -R"
		NO_OUTPUT=""

		rm -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/music
		rm -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/radio
		rm -R /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/playlist

		NO_OUTPUT="1"
		printf " ${INFO} link folder music. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/music/TS3MusicBot_${BOTC2} /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/music" "ls -s"

		printf " ${INFO} link folder radio. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/radio/TS3MusicBot_${BOTC2} /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/radio" "ls -s"

		printf " ${INFO} link folder playlist. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/playlist/TS3MusicBot_${BOTC2} /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/playlist" "ls -s"
		NO_OUTPUT=""
	else
		if [ -h /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/music ]; then rm /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/music; fi
		if [ -h /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/radio ]; then rm /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/radio; fi
		if [ -h /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/playlist ]; then rm /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/playlist; fi

		NO_OUTPUT="1"
		printf " ${INFO} link folder music. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/music /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/" "ls -s"

		printf " ${INFO} link folder radio. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/radio /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/" "ls -s"

		printf " ${INFO} link folder playlist. \n"
		spinner & spinner_background "ln -s /home/${USERNAME}/playlist /home/${USERNAME}/system/bot_files/TS3MusicBot_${BOTC2}/" "ls -s"
		NO_OUTPUT=""
	fi

	if [ "${NUMBER[$BOTC2]}" = "-number 1" ]; then
		NUMBER[$BOTC2]="1"
	elif [ "${NUMBER[$BOTC2]}" = "-number 2" ]; then
		NUMBER[$BOTC2]="2"
	elif [ "${NUMBER[$BOTC2]}" = "-number 3" ]; then
		NUMBER[$BOTC2]="3"
	elif [ "${NUMBER[$BOTC2]}" = "-number 4" ]; then
		NUMBER[$BOTC2]="4"
	elif [ "${NUMBER[$BOTC2]}" = "-number 5" ]; then
		NUMBER[$BOTC2]="5"
	elif [ "${NUMBER[$BOTC2]}" = "-number 6" ]; then
		NUMBER[$BOTC2]="6"
	fi

	if [ "${QUERY[$BOTC2]}" = "-query" ]; then
		QUERY[$BOTC2]="true"
	elif [ "${QUERY[$BOTC2]}" = "-noquery" ]; then
		QUERY[$BOTC2]="false"
	fi

	if [ "${BETA[$BOTC2]}" = "-beta" ]; then
		BETA[$BOTC2]="true"
	else
		BETA[$BOTC2]="false"
	fi

	PORT="$BOTC2"
	ACCOUNT="${EMAIL[$BOTC2]}"
	WEBIF_PW=$(echo -n "${MASTERPW[$BOTC2]}" | base64)
	WEBIF_PW_USER=$(echo -n "${USERPW[$BOTC2]}" | base64)
	QUERY="${QUERY[$BOTC2]}"
	MAX_DISK_SPACE="${MAXDISKSPACE[$BOTC2]}"
	NUMBER="${NUMBER[$BOTC2]}"
	BETA="${BETA[$BOTC2]}"
	CONNECT_OVER_PUBLIC_IP="${WEBIFBINDSTART[$BOTC2]}"
	SECRETKEY="${SECRETKEYSTART[$BOTC2]}"

	if ! sqlite3 /home/${USERNAME}/system/settings.db <<< .schema | grep -qiw 'create table bot_parameter'; then printf " ${INFO} Create Table bot_parameter. \n"; sqlite3 /home/${USERNAME}/system/settings.db  <<END_SQL
CREATE TABLE bot_parameter ( post_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT , port TEXT NOT NULL , account TEXT NOT NULL , webif_pw TEXT NOT NULL , webif_pw_user TEXT NOT NULL , 
query TEXT NOT NULL , max_disk_space TEXT NOT NULL , number TEXT , beta TEXT NOT NULL , connect_over_public_ip TEXT , secretkey TEXT , disable_ytdl TEXT NOT NULL , streaming_port TEXT );
END_SQL
	fi

	printf " ${INFO} Insert Bot Parameter into SQlite table bot_parameter. \n"; sqlite3 /home/${USERNAME}/system/settings.db  <<END_SQL
INSERT INTO bot_parameter ( port , account , webif_pw , webif_pw_user , query , max_disk_space , number , beta , connect_over_public_ip , secretkey , disable_ytdl )
VALUES ( '${PORT}' , '${ACCOUNT}' , '${WEBIF_PW}' , '${WEBIF_PW_USER}' , '${QUERY}' , '${MAX_DISK_SPACE}' , '${NUMBER}' , '${BETA}' , '${CONNECT_OVER_PUBLIC_IP}' , 
'${SECRETKEY}' , '${DISABLE_YTDL}' );
END_SQL

	echo
done

NO_OUTPUT="1"
printf " ${INFO} Set owner ${USERNAME} on /home/${USERNAME} recursive. \n"
spinner & spinner_background "chown -R ${USERNAME} /home/${USERNAME}" "chown"
NO_OUTPUT=""

echo

echo "####################"
echo
printf " ${GOOD} Finish \n"
echo
printf " ${YELLOW}${INFO} Please Check if all datas was migrate Succesfully. If yes, then you can delete the old script path.${MAIN} \n\n"
printf " Now use 'TS3MusicBot_Control' to start the Script. \n"

echo

exit 0