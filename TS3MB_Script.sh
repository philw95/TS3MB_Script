#!/bin/bash

D1=$(readlink -f "$0")
BINARYPATH="$(dirname "${D1}")"
cd "${BINARYPATH}"
DATEINAME="$(basename "${D1}")"
ME=`whoami`

# Version, Settingsversion, Name vom diesem Script und Downloadlink
MODEBETA=""
VERSION="0.6.7"
SETTINGSVERSIONSCRIPT="0.1.2"
NAME="TS3MB_Script${MODEBETA}"
BACKTITLE="${NAME} - ${VERSION}"
HOSTSCRIPTLOCATION="https://gitlab.com/philw95/TS3MB_Script/raw/master"

# Falls settings.conf Existiert Einlesen. Wenn nicht dann Benutzer unter dem das Script ausgeführt werden soll bestimmen.
if [ -f settings.conf ]; then
	source settings.conf
else
	CURRENTUSER=$(whoami)
	USER=$(whiptail --backtitle "${BACKTITLE}" --title "${BACKTITLE} - User" --inputbox "Under what user to the bot run? (The Name of the User (NOT ROOT!):" 8 78 ${CURRENTUSER} --nocancel 3>&1 1>&2 2>&3)
fi

# Befehle als angegebener User Ausführen
as_user() {
	if [ "${ME}" = "${USER}" ] ; then
		bash -c "$1"
	else
		su "${USER}" -c "$1"
	fi
}

# Sprachdatein, License, Link Gitlabversion und Link Gitsettingsversion
DOWNLOADLINKSCRIPT="${HOSTSCRIPTLOCATION}/TS3MB_Script.sh"
DOWNLOADLINKLANGENGLISH="${HOSTSCRIPTLOCATION}/lang/english.txt"
DOWNLOADLINKLANGGERMAN="${HOSTSCRIPTLOCATION}/lang/german.txt"
DOWNLOADLINKLICENSE="${HOSTSCRIPTLOCATION}/LICENSE"
cd /tmp
HTTP200CHECKGITLABVERSION=$(as_user "wget -T 2 -t 1 --server-response ${HOSTSCRIPTLOCATION}/version -O .gitlabversion.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
HTTP200CHECKGITSETTINGSVERSION=$(as_user "wget -T 2 -t 1 --server-response ${HOSTSCRIPTLOCATION}/settingsversion -O .gitsettingsversion.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
cd ${BINARYPATH}

# Archive Name des Bots und Download link des Bots
ARCHIVENAME="TS3MusicBot_v4.5.tar"
DOWNLOADLINKBOT="http://download1.ts3musicbot.net/${ARCHIVENAME}"

# Farben definieren für printf
MAIN='\033[0m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
FLASH='\033[5m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

# Cursor Movement
SAVECURSOR='\033[s'
RESTORECURSOR='\033[u'
MOVECURSOR='\033['				# Full command: \033[<n>A	<n>: Anzahl an X Zeilen oder Spalten  A: Aufwärs
MOVECURSORUP='A'
MOVECURSORDOWN='B'
MOVECURSORFORWARD='C'
MOVECURSORBACKWARD='D'
MOVELINEBEGINN='\r'
ERASEENDOFLINE='\033[K'

# Banner
printf "${BOLD}${LIGHTGREEN} _____  ____  _____  __  __  ____       ____    ____  ____   ___  ____  _____${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}|_   _|/ ___||___ / |  \/  || __ )     / ___|  / ___||  _ \ |_ _||  _ \|_   _|${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  | |  \___ \  |_ \ | |\/| ||  _ \     \___ \ | |    | |_) | | | | |_) | | |${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  | |   ___) |___) || |  | || |_) |     ___) || |___ |  _ <  | | |  __/  | |${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}  |_|  |____/|____/ |_|  |_||____/_____|____/  \____||_| \_\|___||_|     |_|${MAIN}\n"
printf "${BOLD}${LIGHTGREEN}                                 |_____|${MAIN}\n\n"

# Hilfe
help () {

clear
echo "Use: ${0} [Argument] [Option] [Port]"
echo ""
echo "${0}  | Start the Graphical menu of the Script"
echo ""
echo "Available Arguments:"
echo ""
echo "    -B           Beta Mode for TS3MusicBot"
echo "    -d           Debug Mode for this Script"
echo "    -D (Mode)    Debug Mode for TS3MusicBot"
echo "                  Available Modes:"
echo "                  -debug-exec         | Print All Debug output"
echo "                  -debug-exec-client  | Print Client Debug output Only"
echo "                  -debug-exec-player  | Print Player Debug output Only"
echo ""
echo "    -p           Skip Packet Check (If you will Skip Always, create file \"nopacketcheck\")"
#echo "    -N           TS3MusicBot start in Notportable Mode"
echo "    -h           Display this help page"
echo ""
echo "Available Options:"
echo ""
echo "    start   (Port)   Start the TS3MusicBot *"
echo "    stop    (Port)   Stop the TS3MusicBot *"
echo "    restart (Port)   Restart the TS3MusicBot *"
echo "    status           Display the Status of All TS3Music Bots"
echo "    update           Check if a Newer Version of the Script is Available"
echo "    help             Display this help page"
echo "    menu             Start the Graphical menu of the Script"
echo ""
echo "  *Note: Not indicated Port are all bots start, stops or restart."
echo ""
exit 0

}

# Arguments
while getopts ":BdD:Nph" opt
do
	case $opt in
		B)
			# Beta Mode for TS3MusicBot
			BETAGLOBAL="1"
			printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}NOTE${MAIN}] TS3MusicBot starting with argument: -beta\n"
		;;
		d)
			# Debug Modus für dieses Script
			DEBUG="1"
			printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}NOTE${MAIN}] ${DATEINAME} starting in Debug Mode\n"
		;;
		D)
			# Debug Modus für TS3MusicBot
			DEBUGGG=$OPTARG
			if [ "${DEBUGGG}" = "-debug-exec" ] || [ "${DEBUGGG}" = "-debug-exec-client" ] || [ "${DEBUGGG}" = "-debug-exec-player" ]; then
				printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}NOTE${MAIN}] TS3MusicBot starting with Debug Mode: ${DEBUGGG}\n"
			else
				printf "${LIGHTCYAN}[$0]${MAIN} ${RED}ERROR:${MAIN} Wrong Debug Mode: ${DEBUGGG} (-h for help)\n"
				exit 1
			fi
		;;
		N)
			# Bot im Notportable Mode Starten
			STARTNOTPORTABLE="1"
			printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}NOTE${MAIN}] TS3MusicBot starting with argument: -notpotable\n"
		;;
		p)
			# Packet Check Überspringen
			PACKETCHECK="disable"
			printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}NOTE${MAIN}] Skip Packet Check (If you will Skip Always, create file \"nopacketcheck\")\n"
		;;
		h)
			help
		;;
		\?)
			printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] Wrong argument: -$OPTARG (-h for help)\n"
			exit 1
		;;
		:)
			printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] -$OPTARG requires an mode. (-h for help)\n"
			exit 1
		;;
	esac
done

# alle gefundenen Argumente aus $@ entfernen
shift $(($OPTIND-1))

getos () {

if which apt-get 2> /dev/null | grep -qw apt-get; then
        if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] Detected OS: Debian/Ubuntu\n"; fi
        OS="debian"
elif which yum 2> /dev/null | grep -qw yum; then
        if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] Detected OS: CentOS/RedHat/Fedora/Suse Linux/other... \n"; fi
        OS="redhat"
else
		printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] Detected OS: ${RED}Couldn't detect your OS!${MAIN}\n"
		printf "\n ## ${RED}Please make sure you have install All needed Packages!${MAIN} ##\n\n"
		read -n 1 -s -r -p " [Press any key to continue]"
		printf " [${GREEN}OK${MAIN}]\n"
fi

}

checkdeb () {

if [ -f nopacketcheck ]; then
	if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] Packet Check Disable! Found file \"nopacketcheck\"\n"; fi
	sleep 1
	return
elif [ "$PACKETCHECK" = "disable" ]; then
	sleep 1
	return
fi

PACKETCOUNTER=0
JAVAINSTALLED=""
JAVAVERSION=""
if [ "$OS" = "debian" ]; then
	# Java
	PACKETLISTJAVA=( openjdk-11-jre openjdk-10-jre openjdk-9-jre openjdk-8-jre openjdk-7-jre )
	for PACKET in "${PACKETLISTJAVA[@]}"
	do
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
			if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] $PACKET installed\n"; fi
			JAVAINSTALLED="1"
		fi
	done

	if ! [ "$JAVAINSTALLED" = "1" ]; then
		if ! [ "${ME}" = "root" ]; then
			printf "\n${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] You are not root! Enter Root Password:\n"
			su root -c "apt-get update"
		else
			apt-get update
		fi

		if apt-cache search openjdk | grep -qo "openjdk-[0-9]-jre " || apt-cache search openjdk | grep -qo "openjdk-[0-9][0-9]-jre "; then
			for PACKET in "${PACKETLISTJAVA[@]}"
			do
				if apt-cache search openjdk | grep -qo "$PACKET "; then
					JAVAVERSION="${PACKET}"
					break
				fi
			done
		fi
	fi

	# Packete prüfen, wenn nicht vorhanden fragen ob es installiert werden soll.
	PACKETLIST=( wget whiptail screen dnsutils $JAVAVERSION )
	for PACKET in "${PACKETLIST[@]}"
	do
		if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
			if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] $PACKET installed\n"; fi
		else
			printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] $PACKET not installed!"
			sleep 1
			printf " Install now? (Yes/no) "
			read
			if [ "$REPLY" = "Yes" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "J" ] || [ "$REPLY" = "j" ] || [ "$REPLY" = "Ja" ] || [ "$REPLY" = "ja" ] || [ "$REPLY" = "" ]; then
				PACKETLISTINSTALL+=($PACKET)
				(( PACKETCOUNTER += 1 ))
			else
				printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] $PACKET not Installing.\n"
			fi
		fi
	done

	if ! [ "${PACKETCOUNTER}" = 0 ]; then
		printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] Installing: ${PACKETLISTINSTALL[*]}. Please wait... "
		if ! [ "${ME}" = "root" ]; then
			printf "\n${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] You are not root! Enter Root Password:\n"
			su root -c "apt-get update && apt-get install -y ${PACKETLISTINSTALL[*]} &> /dev/null"
		else
			apt-get install -y ${PACKETLISTINSTALL[@]} &> /dev/null
		fi
		printf "\n"

		for PACKET in "${PACKETLISTINSTALL[@]}"
		do
			if dpkg-query -W -f='${Status} ${Version}\n' $PACKET 2> /dev/null | grep -qw "ok installed"; then
				printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] ${PACKET}: [${GREEN}OK${MAIN}]\n"
			else
				printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] ${PACKET} Can't Install!\n"
				sleep 2
			fi
		done
	fi
	sleep 1
elif [ "$OS" = "redhat" ]; then
	# Java
	PACKETLISTJAVA=( java-1.11.0-openjdk java-1.10.0-openjdk java-1.9.0-openjdk java-1.8.0-openjdk java-1.7.0-openjdk )
	for PACKET in "${PACKETLISTJAVA[@]}"
	do
		if rpm -q $PACKET > /dev/null; then
			if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] $PACKET installed\n"; fi
			JAVAINSTALLED="1"
		fi
	done

	if ! [ "$JAVAINSTALLED" = "1" ]; then
		if ! [ "${ME}" = "root" ]; then
			printf "\n${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] You are not root! Enter Root Password:\n"
			su root -c "yum -y update"
		else
			yum -y update
		fi

		for PACKET in "${PACKETLISTJAVA[@]}"
		do
			if yum list java-* | grep -qm 1 "$PACKET"; then
				JAVAVERSION="${PACKET}"
				break
			fi
		done
	fi

	PACKETLIST=( wget newt screen bind-utils $JAVAVERSION )
	for PACKET in "${PACKETLIST[@]}"
	do
		if rpm -q $PACKET > /dev/null; then
			if [ "$DEBUG" = "1" ]; then printf "${LIGHTCYAN}[$0]${MAIN} [${ORANGE}DEBUG${MAIN}] $PACKET installed\n"; fi
		else
			printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] $PACKET not installed!"
			sleep 1
			printf " Install now? (Yes/no) "
			read
			if [ "$REPLY" = "Yes" ] || [ "$REPLY" = "yes" ] || [ "$REPLY" = "Y" ] || [ "$REPLY" = "y" ] || [ "$REPLY" = "J" ] || [ "$REPLY" = "j" ] || [ "$REPLY" = "Ja" ] || [ "$REPLY" = "ja" ] || [ "$REPLY" = "" ]; then
				PACKETLISTINSTALL+=($PACKET)
				(( PACKETCOUNTER += 1 ))
			else
				printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] $PACKET not Installing.\n"
			fi
		fi
	done

	if ! [ "${PACKETCOUNTER}" = 0 ]; then
		printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] Installing: ${PACKETLISTINSTALL[*]}. Please wait... "
		if ! [ "${ME}" = "root" ]; then
			printf "\n${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] You are not root! Enter Root Password:\n"
			su root -c "yum -y update && yum install -y ${PACKETLISTINSTALL[*]} &> /dev/null"
		else
			yum install -y ${PACKETLISTINSTALL[@]} &> /dev/null
		fi
		printf "\n"

		for PACKET in "${PACKETLISTINSTALL[@]}"
		do
			if rpm -q $PACKET > /dev/null; then
				printf "${LIGHTCYAN}[$0]${MAIN} [${GREEN}Install${MAIN}] ${PACKET}: [${GREEN}OK${MAIN}]\n"
			else
				printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] ${PACKET} Can't Install!\n"
				sleep 2
			fi
		done
	fi
	sleep 1
fi

}

if [ "$1" = "" ] || [ "$1" = "menu" ]; then
	getos
	checkdeb
fi

# Logger
if [ ! -d ${BINARYPATH}/logs ]; then
	as_user "mkdir ${BINARYPATH}/logs 2> /dev/null"
fi
LOGFILE="${BINARYPATH}/logs/logfile.log"
ERRORLOGFILE="${BINARYPATH}/logs/errorlogfile.log"
if ! [ -f $LOGFILE ]; then
	as_user "touch ${LOGFILE}"
	as_user "chmod 666 ${LOGFILE}"
fi
if ! [ -f $ERRORLOGFILE ]; then
	as_user "touch ${ERRORLOGFILE}"
	as_user "chmod 666 ${ERRORLOGFILE}"
fi

logg () {

if [ -n "`echo ${1} | grep \"ERROR\"`" ]; then
	printf "$(date +%d.%m.%y_%H:%M:%S) " >> $ERRORLOGFILE
	printf "${2}\n" >> $ERRORLOGFILE
fi

if [ "${1}" = "LOGGLEERZEILE" ]; then
	printf "\n" >> $LOGFILE
elif [ "${DEBUG}" = "1" ]; then
	printf "$(date +%d.%m.%y_%H:%M:%S) " >> $LOGFILE
	printf "${2}\n" >> $LOGFILE
else
	if [ "${1}" = "NORMAL" ] || [ "${1}" = "ERROR" ]; then
		printf "$(date +%d.%m.%y_%H:%M:%S) " >> $LOGFILE
		printf "${2}\n" >> $LOGFILE
	fi
fi

}

logg "LOGGLEERZEILE" ""
logg "NORMAL" "==== Start ===="
logg "NORMAL" " ${NAME} (Version ${VERSION} ; Settingsversion ${SETTINGSVERSION})"
if [ "$DEBUG" = "1" ]; then logg "" " Info: Start with Debug Mode"; fi

# Script nur als angegebener User oder Root ausführen
if ! [ "${ME}" = "${USER}" ] && ! [ "${ME}" = "root" ]; then
	if [ "${ME}" = "root" ]; then
		whiptail --backtitle "${BACKTITLE}" --title "${BACKTITLE} - User" --msgbox "Please run this Script as root!" 8 78 3>&1 1>&2 2>&3
	else
		whiptail --backtitle "${BACKTITLE}" --title "${BACKTITLE} - User" --msgbox "Please run this Script as $USER or root!" 8 78 3>&1 1>&2 2>&3
	fi
	logg "ERROR" " ERROR: Script start with the wrong User!"
	logg "NORMAL" "==== END (USER) ===="
	exit 1
fi

# Sprache Einstellen
languagemenu () {

if ! [ "${LANGUAGE}" = "" ]; then
	if [ "${LANGUAGE}" = "english" ]; then
		LANGUAGE1="ON"
	else
		LANGUAGE1="OFF"
	fi

	if [ "${LANGUAGE}" = "german" ]; then
		LANGUAGE2="ON"
	else
		LANGUAGE2="OFF"
	fi
else LANGUAGE1="ON" LANGUAGE2="OFF"
fi

LANGUAGE=$(whiptail --backtitle "${BACKTITLE}" --title "Language" --nocancel --radiolist \
		"Set your language:\n(using the space bar)" 13 48 2 \
		"english" "English" $LANGUAGE1 \
		"german" "Deutsch" $LANGUAGE2 \
		3>&1 1>&2 2>&3)
source ${BINARYPATH}/lang/${LANGUAGE}.txt
logg "NORMAL" " Set Language: $LANGUAGE"

}

# Sprachdatein Herunterladen falls nicht vorhanden
if [ ! -d lang ] || [ ! -f ${BINARYPATH}/lang/english.txt ] || [ ! -f ${BINARYPATH}/lang/german.txt ];then
	ERROR=""
	logg "" " Download the Language files"
	if [ ! -d lang ]; then
		as_user "mkdir lang"; if ! [ -d lang ]; then printf "${RED}Folder Lang not Created!. Please try again!${MAIN}\n"; logg "ERROR" "  ERROR: Folder lang not Created!"; logg "NORMAL" "==== END (Lang Download) ===="; exit 1; fi
	fi
	cd ${BINARYPATH}/lang
	printf "${LIGHTBLUE}Please wait. Language files will be downloaded...${MAIN}\n"
	a="0"
	while [ "$a" -lt "5" ]; do
		(( a += 1 ))
		cd /tmp
		HTTP200CHECKLANGENGLISH=$(as_user "wget -T 2 -t 1 --server-response ${DOWNLOADLINKLANGENGLISH} -O .english.txt.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
		HTTP200CHECKLANGGERMAN=$(as_user "wget -T 2 -t 1 --server-response ${DOWNLOADLINKLANGGERMAN} -O .german.txt.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
		cd ${BINARYPATH}/lang

		if [ "${HTTP200CHECKLANGENGLISH}" = "1" ] && [ "${HTTP200CHECKLANGGERMAN}" = "1" ]; then
			if [ ! -f ${BINARYPATH}/lang/english.txt ]; then
				as_user "mv /tmp/.english.txt.temp ${BINARYPATH}/lang/english.txt"; if ! [ -f ${BINARYPATH}/lang/english.txt ]; then logg "ERROR" "  ERROR: english.txt not Moved!"; ERROR="1"; fi
			fi
			if [ ! -f ${BINARYPATH}/lang/german.txt ]; then
				as_user "mv /tmp/.german.txt.temp ${BINARYPATH}/lang/german.txt"; if ! [ -f ${BINARYPATH}/lang/german.txt ]; then logg "ERROR" "  ERROR: german.txt not Moved!"; ERROR="1"; fi
			fi
			if ! [ "${ERROR}" = "1" ]; then logg "" " Language Files Successfully Downloaded"; else logg "ERROR" " ERROR: Language File not Downloaded!"; fi
			a=""
			break
		fi
	done
	cd ${BINARYPATH}
	if [ "${ERROR}" = "1" ]; then
		printf "${RED}Language file can´t be downloaded. Please try again!${MAIN}\n"
		logg "ERROR" " ERROR: Can´t Donwload the Language Files!"
		logg "NORMAL" "==== END (Language) ===="
		exit 1
	fi
fi
if [ -f settings.conf ]; then
	source settings.conf
	if [ "${LANGUAGE}" = "" ];then
		languagemenu
	fi
	source ${BINARYPATH}/lang/${LANGUAGE}.txt
	logg "NORMAL" " Set Language: $LANGUAGE"
else
	languagemenu
fi

# for whiptail
MENU="${TEXTMENU01}"
TITLEUPDATE="${BACKTITLE} - ${TEXTMENU02}"
TITLESETTINGSUPDATE="${BACKTITLE} - ${TEXTMENU03}"
TITLEHAUPTMENU="${BACKTITLE} - ${TEXTMENU04}"
TITLECREATECONFIG="${BACKTITLE} - ${TEXTMENU05}"
TITLECREATEBOT="${BACKTITLE} - ${TEXTMENU06}"
TITLESCREENSESSION="${BACKTITLE} - ${TEXTMENU07}"
TITLEEXTRAS="${BACKTITLE} - ${TEXTMENU08}"
TITLEEXTRASAUTOSTART="${BACKTITLE} - ${TEXTMENU09}"
TITLEEXTRASAUTORESTART="${BACKTITLE} - ${TEXTMENU10}"
TITLEBOTDATENAENDERN="${BACKTITLE} - ${TEXTMENU11}"
TITLEUSER="${BACKTITLE} - ${TEXTMENU12}"
TITLESTARTBOT="${BACKTITLE} - ${TEXTMENU13}"
TITLESTOPBOT="${BACKTITLE} - ${TEXTMENU14}"
TITLERESTARTBOT="${BACKTITLE} - ${TEXTMENU15}"
TITLEDELETEBOT="${BACKTITLE} - ${TEXTMENU16}"
TITELBOTINFO="${BACKTITLE} - ${TEXTMENU17}"

# Version check
vcheck () {

logg "NORMAL" " ---------------"
logg "NORMAL" " Update Check"
if [ "${HTTP200CHECKGITLABVERSION}" = "1" ] && [ "${HTTP200CHECKGITSETTINGSVERSION}" = "1" ]; then
	as_user "mv /tmp/.gitlabversion.temp ${BINARYPATH}/.gitlabversion"; if [ ! -f .gitlabversion ]; then logg "ERROR" "  ERROR: .gitlabversion not Moved!"; fi
	as_user "mv /tmp/.gitsettingsversion.temp ${BINARYPATH}/.gitsettingsversion"; if [ ! -f .gitsettingsversion ]; then logg "ERROR" "  ERROR: .gitsettingsversion not Moved!"; fi
	if [ "${VERSION}" = "$(cat .gitlabversion)" ]; then
		logg "NORMAL" "  Gitlabversionscheck: OK"
	else
		logg "NORMAL" "  Gitlabversionscheck: UPDATE Available"
	fi
	logg "NORMAL" "  Gitlabversion: $(cat .gitlabversion)"
	logg "NORMAL" "  Gitlabsettingsversion: $(cat .gitsettingsversion)"
else
	as_user "rm /tmp/.gitlabversion.temp /tmp/.gitsettingsversion.temp"
	printf "\n${RED} Cannot check if an new Version is Available!${MAIN}"
	VERSIONSCHECKFAIL="1"
	logg "ERROR" "  Gitlabversionscheck: ERROR"
	logg "ERROR" "  ERROR: ${NAME} is Not up to date!!"
	read -n 1 -s -r -p " [Press any key to continue]"
	printf "\n"
fi

GITLABVERSION="$(cat .gitlabversion)"
GITSETTINGSVERSION="$(cat .gitsettingsversion)"
if ! [ -f .gitlabversion ] || ! [ -f .gitsettingsversion ]; then
	GITLABVERSION="${VERSION}"
	GITSETTINGSVERSION="${SETTINGSVERSIONSCRIPT}"
	logg "" "  Set old versionsnumber!"
fi


if [ "${VERSION}" = "${GITLABVERSION}" ]; then
	if ! [ "${VERSIONSCHECKFAIL}" = "1" ]; then
		printf "${GREEN}${NAME} ${TEXTUPDATE01} ${VERSION}${MAIN}\n"
		logg "NORMAL" "  ${NAME} is up to date: Version ${VERSION}"
	fi
	sleep 1
else
	if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEUPDATE}" --yesno "${TEXTUPDATE02}\n(Version ${GITLABVERSION})" 9 78) then
		logg "NORMAL" "  Script update to version: ${GITLABVERSION}"
		a="0"
		while [ "$a" -lt "5" ]; do
			(( a += 1 ))
			cd /tmp
			HTTP200CHECKTS3MBSCRIPT=$(as_user "wget -T 2 -t 1 --server-response ${DOWNLOADLINKSCRIPT} -O TS3MB_Script.sh.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
			HTTP200CHECKLICENSE=$(as_user "wget -T 2 -t 1 --server-response ${DOWNLOADLINKLICENSE} -O license.temp 2>&1| grep -c 'HTTP/1.1 200 OK'")
			cd ${BINARYPATH}

			if [ "${HTTP200CHECKTS3MBSCRIPT}" = "1" ]; then
				as_user "rm ${DATEINAME}"; if [ -f ${DATEINAME} ]; then logg "ERROR" "  ERROR: ${DATEINAME} not Deleted!"; fi
				as_user "rm -R lang/"; if [ -d lang ]; then logg "ERROR" "  ERROR: lang folder not Deleted!"; fi
				as_user "mv /tmp/TS3MB_Script.sh.temp ${BINARYPATH}/${DATEINAME}"; if [ ! -f ${DATEINAME} ]; then logg "ERROR" "  ERROR: ${DATEINAME} not Moved!"; fi
				if [ "${HTTP200CHECKLICENSE}" = "1" ]; then
					as_user "rm license"; if [ -f license ]; then logg "ERROR" "  ERROR: license not Deleted!"; fi
					as_user "mv /tmp/license.temp ${BINARYPATH}/license"; if [ ! -f license ]; then logg "ERROR" "  ERROR: license not Moved!"; fi
				fi
				as_user "chmod +x ${DATEINAME}"
				logg "NORMAL" "  Start new file"
				logg "NORMAL" " ---------------"
				logg "NORMAL" "==== END (vcheck) ===="
				exec ./${DATEINAME}
			fi
		done

		printf "${RED}ERROR. Please Check if ${DOWNLOADLINKSCRIPT} is reachable!${MAIN}\n"
		as_user "rm -R /tmp/TS3MB_Script.sh.temp"
		logg "ERROR" "  ERROR: Can´t Download the Script!"
		logg "NORMAL" " ---------------"
		logg "NORMAL" "==== END (vcheck) ===="
		exit 1
	fi
logg "NORMAL" "  Script will not be updated"
fi

logg "NORMAL" " Update Check End"
logg "NORMAL" " ---------------"

}

# Konfig Erstellen
createconfig () {

logg "" " Create Config"

	# Bestimmen ob die Gleichen Radio und Musik Datein in allen Bots verwendet werden
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATECONFIG}" --yesno "${TEXTCREATECONFIG02}" 8 78 --defaultno) then
			MUSICANDRADIOFOLDER="1"
		else
			MUSICANDRADIOFOLDER="0"
		fi

	# Webinterface Check Aktivieren oder Deaktivieren
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATECONFIG}" --yesno "${TEXTCREATECONFIG03}" 8 78) then
			WEBIFCHECK="1"
		else
			WEBIFCHECK="0"
		fi

	# Wartezeit beim Server Start
		BOOTTIME="30"

	# Wartezeit beim Restart
		RESTARTTIME="1"

	createconfigsave

}

createconfigsave () {

logg "" " Config Save"
	as_user "echo \"# This is the Config File for ${NAME}.\" > settings.conf"
	as_user "echo \"#\" >> settings.conf"
	as_user "echo \"# Global Settings\" >> settings.conf"
	as_user "echo \"SETTINGSVERSION='${SETTINGSVERSIONSCRIPT}'\" >> settings.conf"
	as_user "echo \"LANGUAGE='${LANGUAGE}'\" >> settings.conf"
	as_user "echo \"USER='${USER}'\" >> settings.conf"
	as_user "echo \"MUSICANDRADIOFOLDER='${MUSICANDRADIOFOLDER}'\" >> settings.conf"
	as_user "echo \"WEBIFCHECK='${WEBIFCHECK}'\" >> settings.conf"
	as_user "echo \"BOOTTIME='${BOOTTIME}'\" >> settings.conf"
	as_user "echo \"RESTARTTIME='${RESTARTTIME}'\" >> settings.conf"
	as_user "echo \"SHORTCUT='${SHORTCUT}'\" >> settings.conf"
	as_user "echo \"\" >> settings.conf"
	as_user "echo \"\" >> settings.conf"

	if [ -f settings.conf ]; then logg "" "  settings.conf Successfully Created"; else logg "ERROR" "  ERROR: settings.conf Not Created!"; fi

	source settings.conf

}

# Prüfen ob settings.conf Existiert. Wenn nicht dann Erstellen
if [ ! -f settings.conf ]; then
	logg "" " settings.conf not exist. Create it now"
	createconfig
else logg "" " source settings.conf"; source settings.conf
fi

# Versions Check wird nur ausgeführt wenn das Graphiche Menü gestartet wird
if [ "$1" = "" ] || [ "$1" = "menu" ]; then
	vcheck

	whiptail --backtitle "${BACKTITLE}" --title "TS3MusicBot_Control" --msgbox "\n This Script is not longer Supported! \n\n New Script TS3MusicBot_Control Available! \n\n If you will Upgrade please go to path: \n ${BINARYPATH} \n\n Then Run this Command: \n curl -sSL http://y.pkhw.de/update | bash \n\n More Informations here: http://y.pkhw.de/ts3mbcontrol \n\n                         It takes some minutes!" 20 78
fi

# Botdaten Erstellen
createbot () {

logg "" " ----------"
logg "" " Create Bot"

	# E-Mail Addresse
		EMAIL=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT02}" 8 78 --nocancel 3>&1 1>&2 2>&3)

	# Admin Passwort
		MASTERPW=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT03}" 8 78 --nocancel 3>&1 1>&2 2>&3)

	# User Passwort
		USERPW=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT04}" 8 78 --nocancel 3>&1 1>&2 2>&3)

	# Port
		portcreate

	# Query
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTCREATEBOT09}" 8 78 --defaultno) then
			QUERY="-query"
		else
			QUERY="-noquery"
		fi

	# Speicherplatz Radio und Musik Ordner
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTCREATEBOT23}" 8 78 --defaultno) then
			MAXDISKSPACE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT10}" 8 78 10240 --nocancel 3>&1 1>&2 2>&3)
		else
			MAXDISKSPACE="-1"
		fi

	# Konfigurationsnummer
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno --defaultno "${TEXTCREATEBOT11}" 8 78) then
			NUMBER="-"$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --nocancel --radiolist \
			"${TEXTCREATEBOT12}" 15 48 6 \
			"number 1" "${TEXTCREATEBOT13}" OFF \
			"number 2" "${TEXTCREATEBOT14}" OFF \
			"number 3" "${TEXTCREATEBOT15}" OFF \
			"number 4" "${TEXTCREATEBOT26}" OFF \
			"number 5" "${TEXTCREATEBOT27}" OFF \
			"number 6" "${TEXTCREATEBOT28}" OFF \
			3>&1 1>&2 2>&3)
		else
			NUMBER=""
		fi

	# Beta
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTCREATEBOT16}" 10 78 --defaultno) then
			BETA="-beta"
		else
			BETA=""
		fi

	# Webinterface Bind IP
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTCREATEBOT17}" 8 78 --defaultno) then
			WEBIFBIND=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT18}" 8 78 --nocancel 3>&1 1>&2 2>&3)
		else
			WEBIFBIND=""
		fi

	# Secret key (Hoster API)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTCREATEBOT19}" 8 78 --defaultno) then
			SECRETKEY=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT20}" 8 78 --nocancel 3>&1 1>&2 2>&3)
		else
			SECRETKEY=""
		fi

	# Screen NAME
		(( BOTS +=1 ))
		SCREENNAME="TS3MusicBot${MODEBETA}_${PORT}"

	clear
	printf "${LIGHTBLUE}${TEXTCREATEBOT01}${MAIN}\n"
	sleep 2

	BOTC2="${PORT}"
	downloadbot

	# Speichern der Botdaten in settings.conf
	logg "" "  Save Bot Information into settings.conf"
	if [ ! -w settings.conf ]; then logg "ERROR" "   ERROR: settings.conf is not writable!"; fi
	as_user "echo \"# Bot ${PORT}\" >> settings.conf"
	as_user "echo \"EMAIL[${PORT}]='${EMAIL}'\" >> settings.conf"
	as_user "echo \"MASTERPW[${PORT}]='${MASTERPW}'\" >> settings.conf"
	as_user "echo \"USERPW[${PORT}]='${USERPW}'\" >> settings.conf"
	as_user "echo \"PORT[${PORT}]='${PORT}'\" >> settings.conf"
	as_user "echo \"QUERY[${PORT}]='${QUERY}'\" >> settings.conf"
	as_user "echo \"MAXDISKSPACE[${PORT}]='${MAXDISKSPACE}'\" >> settings.conf"
	as_user "echo \"NUMBER[${PORT}]='${NUMBER}'\" >> settings.conf"
	as_user "echo \"BETA[${PORT}]='${BETA}'\" >> settings.conf"
	as_user "echo \"WEBIFBIND[${PORT}]='${WEBIFBIND}'\" >> settings.conf"
	as_user "echo \"SECRETKEY[${PORT}]='${SECRETKEY}'\" >> settings.conf"
	as_user "echo \"SCREENNAME[${PORT}]='${SCREENNAME}'\" >> settings.conf"
	as_user "echo \"\" >> settings.conf"

	source settings.conf

	logg "" "  Save Port into bots.ini"
	if [ ! -w bots.ini ]; then logg "ERROR" "   ERROR: bots.ini is not writable!"; fi
	as_user "echo \"${PORT}\" >> bots.ini"

	logg "" "  sorting bots.ini"
	as_user "sort --numeric-sort bots.ini -o bots.ini"

	BOTA=($(< bots.ini ))
	BOTS=${#BOTA[@]}
	(( BOTS -=1 ))
	logg "" "  Create Bot End"
	logg "" " ----------"

}

portcreate () {

PORT=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTCREATEBOT05}" 8 78 --nocancel 3>&1 1>&2 2>&3)

if [ ! -z "$(echo ${PORT} |tr -d '[:digit:]')" ]; then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTCREATEBOT24}" 10 78
	portcreate
fi

if [ "${PORT}" -le "1023" ];then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTCREATEBOT06}" 10 78
	portcreate
elif [ "${PORT}" = "" ];then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTCREATEBOT07}" 10 78
	portcreate
else
	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if ! [ "${BOTC2}" = "" ]; then
			if [ "${PORT[$BOTC2]}" = "${PORT}" ];then
				whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTCREATEBOT08}" 10 78
				portcreate
			fi
		fi
	done

fi

}

downloadbot () {

logg "" " -----"
logg "" " Download Bot"
ERROR=""
	a="0"
	while [ "$a" -lt "5" ]; do
		(( a += 1 ))
		cd /tmp
		HTTP200CHECKTS3MUSICBOT=$(as_user "wget -T 2 -t 1 --server-response ${DOWNLOADLINKBOT} -O ${ARCHIVENAME} 2>&1| grep -c 'HTTP/1.1 200 OK'")
		cd ${BINARYPATH}

		if [ "${HTTP200CHECKTS3MUSICBOT}" = "1" ]; then
			logg "" "  Unzip ${ARCHIVENAME}"
			as_user "tar xfv /tmp/${ARCHIVENAME} -C ${BINARYPATH}/ > /dev/null"; if [ -d TS3MusicBot ]; then logg "" "   Successfully"; else logg "ERROR" "   ERROR: ${ARCHIVENAME} File not unzip!"; ERROR="1"; fi
			logg "" "  Delete ${ARCHIVENAME}"
			as_user "rm /tmp/${ARCHIVENAME} > /dev/null"; if [ ! -f ${ARCHIVENAME} ]; then logg "" "   Successfully"; else logg "ERROR" "   ERROR: ${ARCHIVENAME} File not Deleted!"; ERROR="1"; fi
			logg "" "  Move TS3MusicBot to TS3MusicBot_${BOTC2}"
			as_user "mv TS3MusicBot TS3MusicBot_${BOTC2}"; if [ -d TS3MusicBot_${BOTC2} ]; then logg "" "   Successfully"; else logg "ERROR" "   ERROR: TS3MusicBot Folder not Moved!"; ERROR="1"; fi
			if [ "${ERROR}" = "1" ]; then logg "ERROR" "  ERROR: Download Bot Not Successfully"; else logg "" "  Download Bot Successfully"; fi
			a=""
			break
		fi
	done

	if ! [ -d "TS3MusicBot_${BOTC2}" ]; then
		logg "ERROR" "  ERROR: TS3MusicBot could not be downloaded!"
	fi

	if [ "${MUSICANDRADIOFOLDER}" = "1" ]; then
		if [ ! -d music ]; then
			logg "" " Create the folder: music"
			as_user "mkdir music "
			if [ -d music ]; then logg "" "  Create the folder: music Successfully"; else logg "ERROR" "  ERROR: music folder Not Created"; fi
		fi
		if [ ! -d radio ]; then
			logg "" " Create the folder: radio"
			as_user "mkdir radio"
			if [ -d radio ]; then logg "" "  Create the folder: radio Successfully"; else logg "ERROR" "  ERROR: radio folder Not Created"; fi
		fi
		if [ ! -d playlist ]; then
			logg "" " Create the folder: playlist"
			as_user "mkdir playlist"
			if [ -d playlist ]; then logg "" "  Create the folder: playlist Successfully"; else logg "ERROR" "  ERROR: playlist folder Not Created"; fi
		fi

		logg "" " link music, radio and playlist folder"
		as_user "rm -R $BINARYPATH/TS3MusicBot_${BOTC2}/music 2> /dev/null"
		if [ -d ${BINARYPATH}/TS3MusicBot_${BOTC2}/music ]; then
			printf "${RED}ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/music not Deleted! music folder Not Linked!${MAIN}\n"
			logg "ERROR" "  ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/music not Deleted! music folder Not Linked!"
		else
			as_user "ln -s $BINARYPATH/music $BINARYPATH/TS3MusicBot_${BOTC2}/ 2> /dev/null"
			if [ -d TS3MusicBot_${BOTC2}/music ]; then
				logg "" "  Link the folder: music Successfully"
			else
				logg "ERROR" "  ERROR: music folder Not Linked!"
			fi
		fi

		as_user "rm -R $BINARYPATH/TS3MusicBot_${BOTC2}/radio 2> /dev/null"
		if [ -d ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio ]; then
			printf "${RED}ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio not Deleted! radio folder Not Linked!${MAIN}\n"
			logg "ERROR" "  ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio not Deleted! radio folder Not Linked!"
		else
			as_user "ln -s $BINARYPATH/radio $BINARYPATH/TS3MusicBot_${BOTC2}/ 2> /dev/null"
			if [ -d TS3MusicBot_${BOTC2}/radio ]; then
				logg "" "  Link the folder: radio Successfully"
			else
				logg "ERROR" "  ERROR: radio folder Not Linked!"
			fi
		fi

		as_user "rm -R $BINARYPATH/TS3MusicBot_${BOTC2}/playlist 2> /dev/null"
		if [ -d ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist ]; then
			printf "${RED}ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist not Deleted! playlist folder Not Linked!${MAIN}\n"
			logg "ERROR" "  ERROR: ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist not Deleted! playlist folder Not Linked!"
		else
			as_user "ln -s $BINARYPATH/playlist $BINARYPATH/TS3MusicBot_${BOTC2}/ 2> /dev/null"
			if [ -d TS3MusicBot_${BOTC2}/playlist ]; then
				logg "" "  Link the folder: playlist Successfully"
			else
				logg "ERROR" "  ERROR: playlist folder Not Linked!"
			fi
		fi
	fi
logg "" " Download Bot End"
logg "" " -----"

}

# settings.conf versions Prüfung
settingsvcheck () {

logg "NORMAL" " ---------------"
logg "NORMAL" " Settingsversionscheck"
if ! [ "${SETTINGSVERSION}" = "${GITSETTINGSVERSION}" ]; then
	logg "NORMAL" "  Settings.conf is Outdated. It will now renewed"
	logg "NORMAL" "Version: ${SETTINGSVERSION}"
	SETTINGSVERSIONOLD="${SETTINGSVERSION}"
	source bots.ini
	whiptail --backtitle "${BACKTITLE}" --title "${TITLESETTINGSUPDATE}" --msgbox "${TEXTSETTINGSUPDATE01}" 8 78 3>&1 1>&2 2>&3

	if [ "${SETTINGSVERSIONOLD}" = "" ]; then
		# Bestimmen ob die Gleichen Radio und Musik Datein in allen Bots verwendet werden
		if [ "${MUSICANDRADIOFOLDER}" = "" ]; then
			if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESETTINGSUPDATE}" --yesno "${TEXTSETTINGSUPDATE02}" 10 78 --defaultno) then
				MUSICANDRADIOFOLDER="1"
			else
				MUSICANDRADIOFOLDER="0"
			fi

			BOTC=0
			while ! [ "${BOTC}" = "${BOTS}" ]; do
				(( BOTC += 1 ))
				BOTC2=${BOTA[$BOTC]}

				as_user "mkdir music"
				as_user "mkdir radio"
				as_user "mkdir playlist"

				if [ "${MUSICANDRADIOFOLDER}" = "1" ]; then
					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot${BOTC}/radio/. ${BINARYPATH}/radio"
					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot${BOTC}/music/. ${BINARYPATH}/music"
					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot${BOTC}/playlist/. ${BINARYPATH}/playlist"
					sleep 2

					as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/music"
					as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/radio"
					as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/playlist"
					as_user "ln -s ${BINARYPATH}/music ${BINARYPATH}/TS3MusicBot${BOTC}/"
					as_user "ln -s ${BINARYPATH}/radio ${BINARYPATH}/TS3MusicBot${BOTC}/"
					as_user "ln -s ${BINARYPATH}/playlist ${BINARYPATH}/TS3MusicBot${BOTC}/"
				fi
			done
		fi
	fi

	if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.5" ]; then
		# Radio und Music Orner Anpassen
		if [ "${MUSICANDRADIOFOLDER}" = "1" ]; then
			if [ ! -d music ]; then
				as_user "mkdir music"
				as_user "mkdir radio"
				as_user "mkdir playlist"
				as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot1/radio/. ${BINARYPATH}/radio"
				as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot1/music/. ${BINARYPATH}/music"
				as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot1/playlist/. ${BINARYPATH}/playlist"
				sleep 2

				as_user "rm -R ${BINARYPATH}/TS3MusicBot1/music"
				as_user "rm -R ${BINARYPATH}/TS3MusicBot1/radio"
				as_user "rm -R ${BINARYPATH}/TS3MusicBot1/playlist"
				as_user "ln -s ${BINARYPATH}/music ${BINARYPATH}/TS3MusicBot1/"
				as_user "ln -s ${BINARYPATH}/radio ${BINARYPATH}/TS3MusicBot1/"
				as_user "ln -s ${BINARYPATH}/playlist ${BINARYPATH}/TS3MusicBot1/"
			fi

			BOTC=1
			while ! [ "${BOTC}" = "${BOTS}" ]; do
				(( BOTC += 1 ))
				as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot${BOTC}/playlist/. ${BINARYPATH}/playlist"
				as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/music"
				as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/radio"
				as_user "rm -R ${BINARYPATH}/TS3MusicBot${BOTC}/playlist"
				sleep 1
				as_user "ln -s ${BINARYPATH}/music ${BINARYPATH}/TS3MusicBot${BOTC}/"
				as_user "ln -s ${BINARYPATH}/radio ${BINARYPATH}/TS3MusicBot${BOTC}/"
				as_user "ln -s ${BINARYPATH}/playlist ${BINARYPATH}/TS3MusicBot${BOTC}/"
			done
		fi
	fi

	if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ]; then
		# RESTARTTIME wert Setzten wenn nicht vorhanden
			if [ "${RESTARTTIME}" = "" ]; then
				RESTARTTIME="1"
			fi
	fi

	if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.5" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.6" ]; then
		# Screen Name ändern auf die Portnummer
		sleep 1
	fi

	if [ "${SETTINGSVERSIONOLD}" = "0.0.6" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.7" ]; then
		# bots.ini sortieren
		as_user "sort --numeric-sort bots.ini -o bots.ini"
	fi

	if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.5" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.6" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.7" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.8" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.9" ]; then
		# Webinterface Check Aktivieren oder Deaktivieren
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESETTINGSUPDATE}" --yesno "${TEXTSETTINGSUPDATE03}" 8 78) then
			WEBIFCHECK="1"
		else
			WEBIFCHECK="0"
		fi
	fi

	createconfigsave

	# WEBIFBIND IP neu Setzen ohne -webif-bind-ip und Botdaten neu setzen

	if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.5" ]; then
		as_user "echo \"#_NOT_EDIT_THIS_FILE!!!\" > bots.ini"
		BOTA=($(< bots.ini ))

		BOTC=0
		while ! [ "${BOTC}" = "${BOTS}" ]; do
			(( BOTC += 1 ))
			BOTC2=${PORT[$BOTC]}

			if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ]; then
				if ! [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
					echo ${WEBIFBIND[$BOTC2]} > temp
					WEBIFBINDNEU=($(< temp ))
					WEBIFBIND[${BOTC2}]=${WEBIFBINDNEU[1]}
					as_user "rm temp"
				fi
			fi

			as_user "echo \"# Bot ${BOTC2}\" >> settings.conf"
			as_user "echo \"EMAIL[${BOTC2}]='${EMAIL[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"MASTERPW[${BOTC2}]='${MASTERPW[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"USERPW[${BOTC2}]='${USERPW[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"PORT[${BOTC2}]='${PORT[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"QUERY[${BOTC2}]='${QUERY[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"MAXDISKSPACE[${BOTC2}]='${MAXDISKSPACE[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"NUMBER[${BOTC2}]='${NUMBER[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"BETA[${BOTC2}]='${BETA[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"WEBIFBIND[${BOTC2}]='${WEBIFBIND[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"SECRETKEY[${BOTC2}]='${SECRETKEY[${BOTC}]}'\" >> settings.conf"
			as_user "echo \"SCREENNAME[${BOTC2}]='TS3MusicBot${MODEBETA}_${BOTC}'\" >> settings.conf"
			as_user "echo \"\" >> settings.conf"

			as_user "echo \"${PORT[${BOTC}]}\" >> bots.ini"

			as_user "mv TS3MusicBot${BOTC} TS3MusicBot_${PORT[${BOTC}]}"

		done

		as_user "sort --numeric-sort bots.ini -o bots.ini"

	else
		BOTA=($(< bots.ini ))
		BOTS=${#BOTA[@]}
		(( BOTS -=1 ))
		BOTA=($(< bots.ini ))

		BOTC=0
		while ! [ "${BOTC}" = "${BOTS}" ]; do
			(( BOTC += 1 ))
			BOTC2=${BOTA[$BOTC]}

		if [ "${SETTINGSVERSIONOLD}" = "0.0.1" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.2" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.3" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.4" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.5" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.6" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.7" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.8" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.9" ] || [ "${SETTINGSVERSIONOLD}" = "0.1.0" ]; then
			# Secretkey abändern
			if ! [ "${SECRETKEY[${BOTC2}]}" = "" ]; then
				echo ${SECRETKEY[${BOTC2}]} > temp
				SECRETKEYNEU=($(< temp ))
				SECRETKEY[${BOTC2}]=${SECRETKEYNEU[1]}
				as_user "rm temp"
			fi
		fi

			as_user "echo \"# Bot ${BOTC2}\" >> settings.conf"
			as_user "echo \"EMAIL[${BOTC2}]='${EMAIL[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"MASTERPW[${BOTC2}]='${MASTERPW[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"USERPW[${BOTC2}]='${USERPW[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"PORT[${BOTC2}]='${PORT[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"QUERY[${BOTC2}]='${QUERY[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"MAXDISKSPACE[${BOTC2}]='${MAXDISKSPACE[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"NUMBER[${BOTC2}]='${NUMBER[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"BETA[${BOTC2}]='${BETA[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"WEBIFBIND[${BOTC2}]='${WEBIFBIND[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"SECRETKEY[${BOTC2}]='${SECRETKEY[${BOTC2}]}'\" >> settings.conf"
			as_user "echo \"SCREENNAME[${BOTC2}]='TS3MusicBot${MODEBETA}_${BOTC2}'\" >> settings.conf"
			as_user "echo \"\" >> settings.conf"

			if [ "${SETTINGSVERSIONOLD}" = "0.0.6" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.7" ] || [ "${SETTINGSVERSIONOLD}" = "0.0.8" ]; then
				# Ordner mit _ z.b. TS3MusicBot_8081
				as_user "mv TS3MusicBot${PORT[${BOTC2}]} TS3MusicBot_${PORT[${BOTC2}]}"
			fi
		done
	fi

	source settings.conf

else logg "NORMAL" "  Settings.conf is up to date: Version ${SETTINGSVERSION}"
fi
logg "NORMAL" " Settingsversionscheck End"
logg "NORMAL" " ---------------"

}

if [ "$1" = "" ] || [ "$1" = "menu" ]; then
	settingsvcheck
fi

# Prüfen ob bots.ini Existiert
botsinicheck () {
	if [ -f "bots.ini" ]; then
		BOTA=($(< bots.ini ))
		BOTS=${#BOTA[@]}
		(( BOTS -=1 ))
	else
		as_user "echo \"#_NOT_EDIT_THIS_FILE!!!\" > bots.ini"; if [ ! -w bots.ini ]; then logg "ERROR" "ERROR: bots.ini is not Created or Writable!"; fi
		BOTA=($(< bots.ini ))
		BOTS=${#BOTA[@]}
		(( BOTS -=1 ))
	fi

}
botsinicheck

# Prüfen ob ein Bot Konfiguriert worden ist
if [ "${BOTS}" = "0" ]; then
	clear
	printf "${LIGHTBLUE}${TEXTCREATEBOT21} ${NAME}. ${TEXTCREATEBOT22}${MAIN}\n"
	sleep 2
	createbot $*
fi

# Starte Alle oder Einzelne Bots
startbotauswahl () {

logg "" " ---------------"
logg "" " Start Bot"
BOTC=""

if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --yesno "${TEXTSTARTBOT01}" 10 78 --yes-button "${TEXTSTARTBOT02}" --no-button "${TEXTSTARTBOT03}") then
	logg "" "  Start All Bots"
	startbot
else
	logg "" "  Start Single Bots"
	CHECKLIST=()
	BOTC=0
	BOTSTARTED=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		cd TS3MusicBot_${BOTC2}/
		if [ ! -f .run ]; then
			CHECKLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")
			(( BOTSTARTED += 1 ))
		fi
		cd ${BINARYPATH}

	done

	if [ "${BOTSTARTED}" = "0" ];then
		return
	fi

	let ARLENGTH=${BOTSTARTED}
	BOTSTART=$(whiptail --checklist --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" "${TEXTSTARTBOT04}" 20 40 12 "${CHECKLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
	as_user "echo ${BOTSTART} > temp"

	BOTSTART2=$(sed 's/\"//g' temp)
	BOTSTART3=( ${BOTSTART2} )
	as_user "rm temp"

	if [ "${BOTSTART3}" = "" ]; then
		logg "" "   No Bot Select"
		logg "" " ---------------"
		logg "NORMAL" "==== END (Start Bot) ===="
		exit 1
	fi

	if [ "${#BOTSTART3[@]}" = "1" ];then
		JOINSCREEN=""
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --yesno --defaultno "${TEXTSTARTBOT05}" 10 78 --yes-button "${TEXTSTARTBOT06}" --no-button "${TEXTSTARTBOT07}") then
			if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --yesno --defaultno "${TEXTSTARTBOT11}" 10 78 --yes-button "${TEXTSTARTBOT12}" --no-button "${TEXTSTARTBOT13}") then
				DEBUGGG="-"$(whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --nocancel --radiolist \
				"${TEXTSTARTBOT14}" 13 65 3 \
				"debug-exec" "${TEXTSTARTBOT15}" OFF \
				"debug-exec-client" "${TEXTSTARTBOT16}" OFF \
				"debug-exec-player" "${TEXTSTARTBOT17}" OFF \
				3>&1 1>&2 2>&3)

				if [ "${DEBUGGG}" = "-" ]; then
					DEBUGGG=""
				fi

				logg "" "   Start with: ${DEBUGGG}"
			fi
			JOINSCREEN=1
			whiptail --backtitle "${BACKTITLE}" --title "${TITLESCREENSESSION}" --msgbox "${TEXTSCREENBOT02}" 15 78
		fi
	fi

	startbot
fi
logg "" " Start Bot End"
logg "" " ---------------"

}

# Entscheidung ob Bots Einzelnt oder Alle gestartet werden
startbot () {

BOTC=0
STARTBOTTESTC=0
STARTBOTTESTC3=0
a=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}
	startbottest[${BOTC2}]=""
	startbottest2[${BOTC2}]=""
done
BOTC=0

clear
if ! [ "$2" = "" ]; then
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if [ "${2}" = "${BOTC2}" ]; then
			BOTC3="1"
		fi
	done

	if ! [ "${BOTC3}" = "1" ]; then
		printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] Bot ${2} doesn't exist!\n"
		logg "ERROR" " ERROR: Bot ${2} doesn't exist!"
		logg "NORMAL" "==== END (START Bot) ===="
		exit 1
	fi

	BOTC2="$2"
	startbot2

	STARTBOTTESTC2=$(( $STARTBOTTESTC + 1 ))

	logg "" " ----------"
	logg "" "  Starting Bots"
	if ! [ "${startbottest[$BOTC2]}" = "" ]; then
		startbot3
	fi

	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "Bitte Warten: "
	fi

	if ! [ "${startbottest[$BOTC2]}" = "" ]; then
		startbot4
	fi

	while true; do
		startbot5
		if [ "${STARTBOTTESTC3}" = "${STARTBOTTESTC}" ]; then
			break
		fi
	done

	logg "" "  Starting Bots End"
	logg "" " ----------"

	BOTC=""

elif ! [ "${BOTSTART3}" = "" ]; then
	BOTC3=0

	while ! [ "${BOTC3}" = "${#BOTSTART3[@]}" ]; do
		BOTC=${BOTSTART3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}
		startbot2
	done
	STARTBOTTESTC2=$(( $STARTBOTTESTC + 1 ))

	BOTC3=0
	logg "" " ----------"
	logg "" "  Starting Bots"
	while ! [ "${BOTC3}" = "${#BOTSTART3[@]}" ]; do
		BOTC=${BOTSTART3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}

		if ! [ "${startbottest[$BOTC2]}" = "" ]; then
			startbot3
		fi
	done

	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "Bitte Warten: "
	fi

	BOTC3=0
	while ! [ "${BOTC3}" = "${#BOTSTART3[@]}" ]; do
		BOTC=${BOTSTART3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if ! [ "${startbottest[$BOTC2]}" = "" ]; then
			startbot4
			if ! [ "${JOINSCREEN}" = "1" ]; then
				sleep 5
			fi
		fi
	done

	while true; do
		startbot5
		if [ "${STARTBOTTESTC3}" = "${STARTBOTTESTC}" ]; then
			break
		fi
	done

	logg "" "  Starting Bots End"
	logg "" " ----------"

	BOTC=""

else
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		startbot2
	done
	STARTBOTTESTC2=$(( $STARTBOTTESTC + 1 ))

	BOTC=0
	logg "" " ----------"
	logg "" "  Starting Bots"
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		if ! [ "${startbottest[$BOTC2]}" = "" ]; then
			startbot3
		fi
	done

	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "Bitte Warten: "
	fi

	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if ! [ "${startbottest[$BOTC2]}" = "" ]; then
			startbot4
			sleep 5
		fi
	done

	while true; do
		startbot5
		if [ "${STARTBOTTESTC3}" = "${STARTBOTTESTC}" ]; then
			break
		fi
	done

	logg "" "  Starting Bots End"
	logg "" " ----------"

	BOTC=""
fi

printf " [${GREEN}OK${MAIN}]\n"

}

startbot2 () {

if [ "${restartbottest}" = "1" ]; then
	if ! [ "${stopbottest[$BOTC2]}" = "" ]; then
		(( STARTBOTTESTC += 1 ))
		startbottest[${BOTC2}]="${STARTBOTTESTC}"
		startbottest2[${BOTC2}]="${STARTBOTTESTC}"
	fi
else
	(( STARTBOTTESTC += 1 ))
	startbottest[${BOTC2}]="${STARTBOTTESTC}"
	startbottest2[${BOTC2}]="${STARTBOTTESTC}"
fi

}

# Prüfen ob Bot bereits läuft. Wenn nicht, dann Starten des Bots mit Prüfung ob Bot Läuft
startbot3 () {

if ! [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
	if [ "${WEBIFBIND[$BOTC2]}" = "0.0.0.0" ]; then
		IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
	else
		IP="${WEBIFBIND[$BOTC2]}"
	fi
else
	IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
fi

if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [ ] Client: [ ] ${LIGHTBLUE}Webinterface: ${MAIN}[ ] ${LIGHTBLUE}${IP}:${PORT[$BOTC2]}${MAIN}\n"
fi

}

startbot4 () {

if as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
	(( STARTBOTTESTC3 += 1 ))
	logg "" "   Bot ${BOTC2}: Info: Bot Already Running"

	ZEILE=0
	ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
	PORTSPALTEN=0
	PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
	SPALTE=0
	SPALTE=$(( 8 + ${PORTSPALTEN} ))
	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C[${YELLOW}${TEXTSTARTBOT08}${MAIN}]\033[K\033[u"
	fi
	startbottest2[${BOTC2}]="0"
else

	if ! [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
		logg "" "   Bot ${BOTC2}: Webinteface Bind to an Specific IP Adress: ${WEBIFBIND[$BOTC2]}"
		WEBIFBINDSTART[$BOTC2]="-connect-over-public-ip "${WEBIFBIND[$BOTC2]}
	fi

	if ! [ "${SECRETKEY[${BOTC2}]}" = "" ]; then
		logg "" "   Bot ${BOTC2}: Bot start with an Secretkey"
		SECRETKEYSTART[$BOTC2]="-secretkey "${SECRETKEY[${BOTC2}]}
	fi

	if [ "${BETAGLOBAL}" = "1" ]; then
		BETA[${BOTC2}]="-beta"
	fi

	if ! [ -d "TS3MusicBot_${BOTC2}" ]; then
		logg "" "   Bot ${BOTC2}: Bot Folder not Exist"
		downloadbot
	fi

	if [ -f notportable ] || [ "${STARTNOTPORTABLE}" = "1" ]; then
		#NotPortable Mode Global
		NOTPORTABLE="-notportable"
		NOTPORTABLEGLOBAL="1"
	else
		NOTPORTABLE=""
		NOTPORTABLEGLOBAL="0"
	fi

	cd TS3MusicBot_${BOTC2}/
	# Falls music, radio und playlist gemeinsam genutzt werden, verlingung neu generieren
	if [ "$MUSICANDRADIOFOLDER" = "1" ]; then
		as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/music"; if [ -d music ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link music dosen't Deleted!"; fi
		as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio"; if [ -d radio ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link radio dosen't Deleted!"; fi
		as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist"; if [ -d playlist ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link playlist dosen't Deleted!"; fi

		as_user "ln -s ${BINARYPATH}/music ${BINARYPATH}/TS3MusicBot_${BOTC2}/"; if [ ! -d music ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link music not Created!"; fi
		as_user "ln -s ${BINARYPATH}/radio ${BINARYPATH}/TS3MusicBot_${BOTC2}/"; if [ ! -d radio ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link radio not Created!"; fi
		as_user "ln -s ${BINARYPATH}/playlist ${BINARYPATH}/TS3MusicBot_${BOTC2}/"; if [ ! -d playlist ]; then logg "ERROR" "Bot ${BOTC2}: ERROR: Folder link playlist not Created!"; fi
	fi

	# Löschen von .notportable und .run falls vorhanden
	if [ -f .notportable ]; then
		logg "" "   Bot ${BOTC2}: File .notportable available. Delete it"
		as_user "rm .notportable 2> /dev/null"
		if [ ! -f .notportable ]; then logg "" "    Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: File not Deleted!"; fi
	fi
	if [ -f .run ]; then
		logg "" "   Bot ${BOTC2}: File .run available. Delete it"
		as_user "rm .run 2> /dev/null"
		if [ ! -f .run ]; then logg "" "    Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: File not Deleted!"; fi
	fi

	if [ "${NOTPORTABLEGLOBAL}" = "0" ]; then
		if [ -f notportable ]; then
			#NotPortable Mode Bot
			NOTPORTABLE="-notportable"
		else
			NOTPORTABLE=""
		fi
	fi

	if [ -f ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log ]; then
		logg "" "   Bot ${BOTC2}: Screen Log Files Exist. Delete it"
		as_user "rm ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log 2> /dev/null"
		as_user	"rm screenlog.0 2> /dev/null"
		if [ ! -f ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log ] && [ ! -f screenlog.0 ]; then logg "" "    Bot ${BOTC2}: Removed Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: Files not Deleted!"; fi
	fi

	#Start Command
	logg "" "  Bot ${BOTC2}: Start Bot"
	as_user "screen -L -AmdS ${SCREENNAME[$BOTC2]} ./TS3MusicBot_runscript.sh -account ${EMAIL[$BOTC2]} -port ${PORT[$BOTC2]} -webif-pw ${MASTERPW[$BOTC2]} -webif-pw-user ${USERPW[$BOTC2]} ${QUERY[$BOTC2]} ${NUMBER[$BOTC2]} -max-disk-space ${MAXDISKSPACE[$BOTC2]} ${BETA[$BOTC2]} ${WEBIFBINDSTART[$BOTC2]} ${SECRETKEYSTART[$BOTC2]} ${DEBUGGG} ${NOTPORTABLE}"
	logg "" "   Bot ${BOTC2}: File .run will be Created"
	as_user "touch .run 2> /dev/null"; if [ -f .run ]; then logg "" "    Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: File not Created!"; fi

	if [ "${JOINSCREEN}" = "1" ];then
		logg "" "   Bot ${BOTC2}: Join into the Screen Session"
		as_user "screen -r ${SCREENNAME[$BOTC2]}"
		if ! [ "${BOOTPRINT}" = "off" ]; then
			printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [${YELLOW}.${MAIN}] Client: [${YELLOW}.${MAIN}] ${LIGHTBLUE}Webinterface: ${MAIN}[ ] ${LIGHTBLUE}${IP}:${PORT[$BOTC2]}${MAIN}\n"
			printf "Bitte Warten: "
		fi
	sleep 1
	a="27"
	fi

logg "" "   Bot ${BOTC2}: Link screenlog.0 to logs folder"
as_user "ln screenlog.0 ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log 2> /dev/null"
if [ -f ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log ]; then logg "" "    Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: File not Linked!"; fi
if [ "${NOTPORTABLE}" = "-notportable" ]; then
	logg "" "   Bot ${BOTC2}: Start in Notportable Mode. Create .notportable File"
	as_user "touch .notportable 2> /dev/null"
	if [ -f .notportable ]; then logg "" "    Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Bot ${BOTC2}: ERROR: File not Created!"; fi
fi

fi
cd ${BINARYPATH}

ZEILE=0
ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
PORTSPALTEN=0
PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
SPALTE=0
SPALTE=$(( 9 + ${PORTSPALTEN} ))
if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${YELLOW}.${MAIN}\033[u"
fi

}

startbot5 () {

(( a += 1 ))
if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "."
fi

BOTC=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}
	BOTC3=0

	if ! [ "${startbottest2[$BOTC2]}" = "0" ] && ! [ "${startbottest2[$BOTC2]}" = "" ]; then
		if as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
			# Prüfen ob der Client gestartet ist
			clientstatus

			if [ "$Clientstatus" = "Starting" ]; then
				ZEILE=0
				ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
				PORTSPALTEN=0
				PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
				SPALTE=0
				SPALTE=$(( 21 + ${PORTSPALTEN} ))
				if ! [ "${BOOTPRINT}" = "off" ]; then
					if [ "${CLIENTSTART[$BOTC2]}" = "" ]; then
						printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${YELLOW}.${MAIN}\033[u"
						CLIENTSTART[${BOTC2}]="1"
					fi
				fi
			elif [ "$Clientstatus" = "OK" ]; then
				ZEILE=0
				ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
				PORTSPALTEN=0
				PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
				SPALTE=0
				SPALTE=$(( 9 + ${PORTSPALTEN} ))
				SPALTE1=0
				SPALTE1=$(( 21 + ${PORTSPALTEN} ))
				if ! [ "${BOOTPRINT}" = "off" ]; then
					if [ "${CLIENTSTART2[$BOTC2]}" = "" ]; then
						printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${GREEN}\u2714${MAIN}\033[u"
						printf "\033[s\033[${ZEILE}A\r\033[${SPALTE1}C${GREEN}\u2714${MAIN}\033[u"
						(( STARTBOTTESTC3 += 1 ))
						CLIENTSTART2[${BOTC2}]="1"
						logg "" "   Bot ${BOTC2}: TS3MusicBot Start Correctly [OK]"
					fi
				fi
			fi

			if [ "${CLIENTSTART2[$BOTC2]}" = "1" ]; then
				if ! [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
					if [ "${WEBIFBIND[$BOTC2]}" = "0.0.0.0" ]; then
						IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
					else
						IP="${WEBIFBIND[$BOTC2]}"
					fi
				else
					IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
				fi

				if ! [ "${BOOTPRINT}" = "off" ]; then
					ZEILE=0
					ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
					PORTSPALTEN=0
					PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
					SPALTE=0
					SPALTE=$(( 39 + ${PORTSPALTEN} ))
					IPSPALTEN=0
					IPSPALTEN=$(printf "${IP}" | wc -c)
					SPALTE1=0
					SPALTE1=$(( 44 + ${PORTSPALTEN} + ${IPSPALTEN} + ${PORTSPALTEN} ))

					if [ "${WEBIFCHECK}" = "1" ]; then
						WEBIFCHECK2=$(wget -T 2 -t 1 --server-response http://${IP}:${PORT[$BOTC2]}/favicon.ico -O - 2>&1| grep -c 'HTTP/1.1 200 OK')
						if [ "${WEBIFCHECK2}" = "1" ]; then
							printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${GREEN}\u2714${MAIN}\033[u"
						else
							printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${RED}X${MAIN}\033[u"
						fi
					fi

					cd ${BINARYPATH}/TS3MusicBot_${BOTC2}/
					if [ -f .notportable ]; then
						printf "\033[s\033[${ZEILE}A\r\033[${SPALTE1}C${LIGHTRED}NotPortable${MAIN}\033[u"
					fi
					cd ${BINARYPATH}
				fi

			fi
		fi

		if [ "$a" = "30" ]; then
			if ! [ "${CLIENTSTART2[$BOTC2]}" = "1" ]; then
				ZEILE=0
				ZEILE=$(( $STARTBOTTESTC2 - ${startbottest[$BOTC2]} ))
				PORTSPALTEN=0
				PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
				SPALTE=0
				SPALTE=$(( 9 + ${PORTSPALTEN} ))
				printf "\033[s\033[${ZEILE}A\r\033[${SPALTE}C${RED}X${MAIN}] ${LIGHTRED}Please check the log File: ${BINARYPATH}/logs/\033[K${MAIN}\033[u"
				(( STARTBOTTESTC3 += 1 ))
				logg "ERROR" "   Bot ${BOTC2}: ERROR: TS3MusicBot Start Not Correctly! Please check the log File: ${BINARYPATH}/logs/"
			fi
		fi
	fi

done
BOTC3=""
sleep 1

}

# Stoppe Alle oder Einzelne Bots
stopbotauswahl () {

logg "" " ---------------"
logg "" " Stop Bot"
BOTC=""

if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTOPBOT}" --yesno "${TEXTSTOPBOT01}" 10 78 --yes-button "${TEXTSTOPBOT02}" --no-button "${TEXTSTOPBOT03}") then
	logg "" "  Stop All Bots"
	stopbot
else
	logg "" "  Stop Single Bots"
	CHECKLIST=()
	BOTC=0
	BOTSTARTED=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		cd TS3MusicBot_${BOTC2}/
		if [ -f .run ]; then
			CHECKLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")
			(( BOTSTARTED += 1 ))
		fi
		cd ${BINARYPATH}

	done

	if [ "${BOTSTARTED}" = "0" ];then
		return
	fi

	let ARLENGTH=${BOTSTARTED}
	BOTSTOP=$(whiptail --checklist --backtitle "${BACKTITLE}" --title "${TITLESTOPBOT}" "${TEXTSTOPBOT04}" 20 40 12 "${CHECKLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
	as_user "echo ${BOTSTOP} > temp"

	BOTSTOP2=$(sed 's/\"//g' temp)
	BOTSTOP3=( ${BOTSTOP2} )
	as_user "rm temp"

	if [ "${BOTSTOP3}" = "" ]; then
		logg "" "   No Bot Select"
		logg "" " ---------------"
		logg "NORMAL" "==== END (Stop Bot) ===="
		exit 1
	fi

	stopbot
fi
logg "" " Stop Bot End"
logg "" " ---------------"

}

# Entscheidung ob Bots Einzelnt oder Alle gestoppt werden
stopbot () {

BOTC=0
STOPBOTTESTC=0
STOPBOTTESTC3=0
a=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}
	stopbottest[${BOTC2}]=""
	stopbottest2[${BOTC2}]=""
done
BOTC=0

if ! [ "$2" = "" ]; then
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if [ "${2}" = "${BOTC2}" ]; then
			BOTC3="1"
		fi
	done

	if ! [ "${BOTC3}" = "1" ]; then
		printf "${LIGHTCYAN}[$0]${MAIN} [${RED}ERROR${MAIN}] Bot ${2} doesn't exist!\n"
		logg "ERROR" " ERROR: Bot ${2} doesn't exist!"
		logg "NORMAL" "==== END (STOP Bot) ===="
		exit 1
	fi

	logg "" " ----------"
	logg "" "  Stopping Bots"
	BOTC2="$2"
	stopbot2
	
	STOPBOTTESTC2=$(( $STOPBOTTESTC + 1 ))
	BOTC=1

	if ! [ "${stopbottest[$BOTC2]}" = "" ]; then
		stopbot3
	fi
	stopbot4
	stopbot5

elif ! [ "${BOTSTOP3}" = "" ]; then
	BOTC3=0
	logg "" " ----------"
	logg "" "  Stopping Bots"
	while ! [ "${BOTC3}" = "${#BOTSTOP3[@]}" ]; do
		BOTC=${BOTSTOP3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}
		stopbot2
	done
	STOPBOTTESTC2=$(( $STOPBOTTESTC + 1 ))

	BOTC3=0
	while ! [ "${BOTC3}" = "${#BOTSTOP3[@]}" ]; do
		BOTC=${BOTSTOP3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}

		if ! [ "${stopbottest[$BOTC2]}" = "" ]; then
			stopbot3
		fi
	done

	stopbot4
	stopbot5

else
	logg "" " ----------"
	logg "" "  Stopping Bots"
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		stopbot2
	done
	STOPBOTTESTC2=$(( $STOPBOTTESTC + 1 ))

	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		if ! [ "${stopbottest[$BOTC2]}" = "" ]; then
			stopbot3
		fi
	done

	stopbot4
	stopbot5

fi
BOTC=""
BOTC3=""

if [ "${FORCEQUIT}" = "1" ]; then
	logg "" "   Info: Foce Quit. At least 1 Bot stopt not Correctly."
	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "\r\033[KBitte Warten: [${RED}FORCE QUIT${MAIN}] "
		sleep 1; printf "."; sleep 1; printf "."; sleep 1; printf "."; sleep 1; printf "."; sleep 1; printf ".";
	else
		sleep 5
	fi

	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if [ "${FORCEQUIT[$BOTC2]}" = "1" ]; then
			as_user "pkill -f 'port ${PORT[$BOTC2]}' -9 &> /dev/null"
		fi
	done
	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2
		printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2;printf ".";sleep 2
	else
		sleep 30
	fi
fi

BOTC=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}
	# IP festlegen
	if [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
		IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
	elif [ "${WEBIFBIND[$BOTC2]}" = "0.0.0.0" ]; then
		IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
	else
		IP="${WEBIFBIND[$BOTC2]}"
	fi
	Last4NumbersOfIP=$(echo ${IP} | sed 's/\.//g' | grep -o '....$' )

	if [ "${stopbottest2[$BOTC2]}" = "0" ]; then
		as_user "pkill -f 'port ${PORT[$BOTC2]}' -9 &> /dev/null"
		as_user "pkill -f 'TS3MusicBot_${USER}_${Last4NumbersOfIP}${PORT[$BOTC2]}' -9 &> /dev/null"
	fi
done

printf "\r\033[KBitte Warten: [${GREEN}OK${MAIN}]\n"
sleep 2

logg "" "  Stopping Bots End"
logg "" " ----------"

}

stopbot2 () {

if as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
	(( STOPBOTTESTC += 1 ))
	logg "" "   Stop Bot ${BOTC2}: Stop Bot"
	as_user "screen -S ${SCREENNAME[$BOTC2]} -X stuff $'quit\nquit\n' &> /dev/null"
	stopbottest[${BOTC2}]="${STOPBOTTESTC}"
	stopbottest2[${BOTC2}]="${STOPBOTTESTC}"
else
	cd TS3MusicBot_${BOTC2}/
	if [ -f .run ]; then
		(( STOPBOTTESTC += 1 ))
		logg "" "   Stop Bot ${BOTC2}: Stop Bot"
		stopbottest[${BOTC2}]="${STOPBOTTESTC}"
		stopbottest2[${BOTC2}]="${STOPBOTTESTC}"
	fi
	cd ${BINARYPATH}
fi

}

# Prüfen ob Bot läuft. Wenn ja, dann Stoppen des Bots mit Prüfung ob Bot gestoppt ist. Wenn nicht, dann Stoppen Erzwingen
stopbot3 () {

if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "TS3MusicBot ${BOTC2}: [${YELLOW}STOPPING${MAIN}]\n"
fi
}

stopbot4 () {

if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "Bitte Warten: "
	fi
}

stopbot5 () {

while true; do
	sleep 1
	if ! [ "${BOOTPRINT}" = "off" ]; then
		printf "."
	fi
	(( a += 1 ))

	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		if ! [ "${stopbottest2[$BOTC2]}" = "0" ] && ! [ "${stopbottest2[$BOTC2]}" = "" ]; then
			if ! as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
				(( STOPBOTTESTC3 += 1 ))
				ZEILE=0
				ZEILE=$(( $STOPBOTTESTC2 - ${stopbottest[$BOTC2]} ))
				if ! [ "${BOOTPRINT}" = "off" ]; then
					if ! [ "${FORCEQUIT[$BOTC2]}" = "1" ]; then
						logg "" "   Stop Bot ${BOTC2}: Bot stopped [OK]"
						printf "\033[s\033[${ZEILE}A \r\033[KTS3MusicBot $BOTC2: [${GREEN}OK${MAIN}]\033[u"
					fi
				fi
				stopbottest2[${BOTC2}]="0"

			elif [ "$a" = "10" ]; then
				logg "" "   Stop Bot ${BOTC2}: Stop Bot second Time"
				if [ "$DEBUG" = "1" ]; then printf "[${ORANGE}DEBUG${MAIN}] Send stop second Time "; fi
				as_user "screen -S ${SCREENNAME[$BOTC2]} -X stuff $'quit\nquit\n' &> /dev/null"

			elif [ "$a" = "25" ]; then
				logg "" "   Stop Bot ${BOTC2}: Bot stopped [ERROR]"
				logg "" "   Stop Bot ${BOTC2}: Force Quit!"
				as_user "screen -p 0 -S ${SCREENNAME[$BOTC2]} -X quit &> /dev/null"
				as_user "screen -wipe &> /dev/null"
				as_user "pkill -f 'port ${PORT[$BOTC2]}' &> /dev/null"
				if ! [ "${BOOTPRINT}" = "off" ]; then
					ZEILE=0
					ZEILE=$(( $STOPBOTTESTC2 - ${stopbottest[$BOTC2]} ))
					if ! [ "${BOOTPRINT}" = "off" ]; then
						printf "\033[s\033[${ZEILE}A \r\033[KTS3MusicBot $BOTC2: [${RED}ERROR${MAIN}] ${RED}${TEXTSTOPBOT06}${MAIN}\033[u"
					fi
				fi
				FORCEQUIT="1"
				FORCEQUIT[$BOTC2]="1"
			fi

			cd TS3MusicBot_${BOTC2}/
			if [ -f .notportable ]; then
				logg "" "   Stop Bot ${BOTC2}: File .notportable available. Delete it"
				as_user "rm .notportable 2> /dev/null"
				if [ ! -f .notportable ]; then logg "" "    Stop Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Stop Bot ${BOTC2}: ERROR: File not Deleted!"; fi
			fi
			if [ -f .run ]; then
				logg "" "   Stop Bot ${BOTC2}: File .run available. Delete it"
				as_user "rm .run 2> /dev/null"
				if [ ! -f .run ]; then logg "" "    Stop Bot ${BOTC2}: Successfully"; else logg "ERROR" "    Stop Bot ${BOTC2}: ERROR: File not Deleted!"; fi
			fi
			cd ${BINARYPATH}
		fi
	done

if [ "${STOPBOTTESTC3}" = "${STOPBOTTESTC}" ]; then
	break
fi
done

}

# Neustart Alle oder Einzelne Bots
restartbotauswahl () {

logg "" " --------------------"
logg "" " Restart Bot"

BOTC=""

if (whiptail --backtitle "${BACKTITLE}" --title "${TITLERESTARTBOT}" --yesno "${TEXTRESTARTBOT01}" 10 78 --yes-button "${TEXTRESTARTBOT02}" --no-button "${TEXTRESTARTBOT03}") then
	logg "" "  Restart All Bots"
	restartbot
else
	logg "" "  Restart Single Bots"
	CHECKLIST=()
	BOTC=0
	BOTSTARTED=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		cd TS3MusicBot_${BOTC2}/
		if [ -f .run ]; then
			CHECKLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")
			(( BOTSTARTED += 1 ))
		fi
		cd ${BINARYPATH}

	done

	if [ "${BOTSTARTED}" = "0" ];then
		return
	fi

	let ARLENGTH=${BOTSTARTED}
	BOTRESTART=$(whiptail --checklist --backtitle "${BACKTITLE}" --title "${TITLERESTARTBOT}" "${TEXTRESTARTBOT04}" 20 40 12 "${CHECKLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
	as_user "echo ${BOTRESTART} > temp"

	BOTSTOP2=$(sed 's/\"//g' temp)
	BOTSTART2=$(sed 's/\"//g' temp)
	BOTSTOP3=( ${BOTSTOP2} )
	BOTSTART3=( ${BOTSTART2} )
	as_user "rm temp"

	if [ "${BOTSTOP3}" = "" ]; then
		logg "" "   No Bot Select"
		logg "" " ---------------"
		logg "NORMAL" "==== END (Restart Bot) ===="
		exit 1
	fi

	if [ "${#BOTSTOP3[@]}" = "1" ];then
		JOINSCREEN=""
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --yesno --defaultno "${TEXTSTARTBOT05}" 10 78 --yes-button "${TEXTSTARTBOT06}" --no-button "${TEXTSTARTBOT07}") then
			if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --yesno --defaultno "${TEXTSTARTBOT11}" 10 78 --yes-button "${TEXTSTARTBOT12}" --no-button "${TEXTSTARTBOT13}") then
				DEBUGGG="-"$(whiptail --backtitle "${BACKTITLE}" --title "${TITLESTARTBOT}" --nocancel --radiolist \
				"${TEXTSTARTBOT14}" 13 65 3 \
				"debug-exec" "${TEXTSTARTBOT15}" OFF \
				"debug-exec-client" "${TEXTSTARTBOT16}" OFF \
				"debug-exec-player" "${TEXTSTARTBOT17}" OFF \
				3>&1 1>&2 2>&3)

				if [ "${DEBUGGG}" = "-" ]; then
					DEBUGGG=""
				fi

				logg "" "   Start with: ${DEBUGGG}"
			fi
			JOINSCREEN=1
			whiptail --backtitle "${BACKTITLE}" --title "${TITLESCREENSESSION}" --msgbox "${TEXTSCREENBOT02}" 10 78
		fi
	fi

	restartbot
fi
logg "" " Restart Bot End"
logg "" " --------------------"

}

# Neustarten Alle oder Einzelne Bots
restartbot () {

restartbottest="1"

if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "${LIGHTPURPLE}${TEXTRESTARTBOT05}${MAIN}\n"
fi

stopbot $*
if ! [ "${BOOTPRINT}" = "off" ]; then
	printf "${YELLOW}${TEXTRESTARTBOT06} (${RESTARTTIME}s)${MAIN}\n"
fi
sleep ${RESTARTTIME}
startbot $*

}

# Status der Bots Anzeigen
statusbot () {

clear
BOTC=0

while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}

	clientstatus

	cd TS3MusicBot_${BOTC2}/

	if as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
		if [ "${Clientstatus}" = "OK" ]; then
			printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [${GREEN}\u2714${MAIN}] Client: [${GREEN}\u2714${MAIN}] ${LIGHTBLUE}Webinterface: ${MAIN}[ ] ${LIGHTBLUE}$IP:${PORT[$BOTC2]}${MAIN}"
		else
			printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [${GREEN}\u2714${MAIN}] Client: [${RED}X${MAIN}] ${LIGHTBLUE}Webinterface: ${MAIN}[ ] ${LIGHTBLUE}$IP:${PORT[$BOTC2]}${MAIN}"
		fi
		if [ "${WEBIFCHECK}" = "1" ]; then
			WEBIFCHECK2=$(wget -T 2 -t 1 --server-response http://${IP}:${PORT[$BOTC2]}/favicon.ico -O - 2>&1| grep -c 'HTTP/1.1 200 OK')
			PORTSPALTEN=0
			PORTSPALTEN=$(printf "${BOTC2}" | wc -c)
			SPALTE=0
			SPALTE=$(( 39 + ${PORTSPALTEN} ))
			if [ "${WEBIFCHECK2}" = "1" ]; then
				printf "\033[s\r\033[${SPALTE}C${GREEN}\u2714${MAIN}\033[u"
			else
				printf "\033[s\r\033[${SPALTE}C${RED}X${MAIN}\033[u"
			fi
		fi

		if [ -f .notportable ]; then
			printf "  ${LIGHTRED}NotPortable${MAIN}\n"
		else
			printf "${MAIN}\n"
		fi
	elif ! [ -f .run ]; then
		printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [${ORANGE}Not started${MAIN}]\n"
	else printf "${LIGHTCYAN}TS3MB ${BOTC2}:${MAIN} [${RED}ERROR${MAIN}] ${LIGHTRED}Please check the log File: ${BINARYPATH}/logs/ ${MAIN}\n"
	fi

	cd ${BINARYPATH}

done

logg "NORMAL" "==== END (Status Bot) ===="
exit 0

}

clientstatus () {

# IP festlegen
if [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
	IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
elif [ "${WEBIFBIND[$BOTC2]}" = "0.0.0.0" ]; then
	IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
else
	IP="${WEBIFBIND[$BOTC2]}"
fi

Last4NumbersOfIP=$(echo ${IP} | sed 's/\.//g' | grep -o '....$' )
TeamspeakClientPid=$(ps ax | grep ${Last4NumbersOfIP}${BOTC2} | grep ts3client_linux | grep -v grep | grep -v '/bin/sh' | awk '{print $1}')
DiscordClientPid=$(ps ax | grep ${Last4NumbersOfIP}${BOTC2} | grep TS3MusicBot_discord | grep -v grep | grep -v '/bin/sh' | awk '{print $1}')

if [ "$TeamspeakClientPid" = "" ] && [ "$DiscordClientPid" = "" ]; then
	Clientstatus="Not Starting"
fi

if ! [ "$TeamspeakClientPid" = "" ]; then
	Clientstatus="Starting"
	TeamspeakClientConnected=$(lsof -ni | grep -E 'ts3client|ld-linux' | grep ${TeamspeakClientPid} | grep -v grep | grep UDP)
	if ! [ "$TeamspeakClientConnected" = "" ]; then
		Clientstatus="OK"
	else
		Clientstatus="ERROR"
	fi
elif ! [ "$DiscordClientPid" = "" ]; then
	Clientstatus="Starting"
	DiscordClientConnected=$(lsof -ni +c 15 | grep TS3MusicBot_dis | grep ${DiscordClientPid} | grep -v grep | grep UDP)
	if ! [ "$DiscordClientConnected" = "" ]; then
		Clientstatus="OK"
	else
		Clientstatus="ERROR"
	fi
fi

}

# Betreten der Screen Sitzung
joinscreen () {

clear

RADIOLIST=()
BOTC=0
BOTSTARTED=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}

	cd TS3MusicBot_${BOTC2}/
	if [ -f .run ]; then
		RADIOLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")
		(( BOTSTARTED += 1 ))
	fi
	cd ${BINARYPATH}

done

if [ "${BOTSTARTED}" = "0" ];then
	return
fi

let ARLENGTH=${BOTSTARTED}
SCREENNUMMER=$(whiptail --radiolist --backtitle "${BACKTITLE}" --title "${TITLESCREENSESSION}" "${TEXTSCREENBOT01}" 20 40 12 "${RADIOLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
as_user "echo ${SCREENNUMMER} > temp"

SCREENNUMMER2=$(sed 's/\"//g' temp)
as_user "rm temp"
BOTC2=${BOTA[$SCREENNUMMER2]}

if [ "${SCREENNUMMER2}" = "" ]; then
	return
fi

whiptail --backtitle "${BACKTITLE}" --title "${TITLESCREENSESSION}" --msgbox "${TEXTSCREENBOT02}" 15 78

logg "" " Join the Screen Session: ${SCREENNAME[$BOTC2]}"
as_user "screen -r ${SCREENNAME[$BOTC2]}"
logg "" " Come back from the Screen Session: ${SCREENNAME[$BOTC2]}"

}

botinfo () {

# Bot Informationen
RADIOLIST=()
BOTC=0
while ! [ "${BOTC}" = "${BOTS}" ]; do
	(( BOTC += 1 ))
	BOTC2=${BOTA[$BOTC]}

	RADIOLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")

done

let ARLENGTH=${BOTS}
BOTINFO=$(whiptail --radiolist --backtitle "${BACKTITLE}" --title "${TITELBOTINFO}" "${TEXTBOTINFO01}" 20 40 12 "${RADIOLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
as_user "echo $BOTINFO > temp"

BOTINFO2=$(sed 's/\"//g' temp)
as_user "rm temp"
BOTC2=${BOTA[$BOTINFO2]}

if [ "${BOTINFO2}" = "" ]; then
	return
fi

if [ "${QUERY[$BOTC2]}" =  "-query" ]; then
	QUERY="${TEXTBOTINFO02}"
else
	QUERY="${TEXTBOTINFO03}"
fi

if ! [ "${NUMBER[$BOTC2]}" = "" ]; then
	if [ "${NUMBER[$BOTC2]}" = "-number 1" ]; then
		NUMBER="1"
	elif [ "${NUMBER[$BOTC2]}" = "-number 2" ]; then
		NUMBER="2"
	elif [ "${NUMBER[$BOTC2]}" = "-number 3" ]; then
		NUMBER="3"
	elif [ "${NUMBER[$BOTC2]}" = "-number 4" ]; then
		NUMBER="4"
	elif [ "${NUMBER[$BOTC2]}" = "-number 5" ]; then
		NUMBER="5"
	elif [ "${NUMBER[$BOTC2]}" = "-number 6" ]; then
		NUMBER="6"
	fi
else
	NUMBER="n.a."
fi

if [ "${BETA[$BOTC2]}" = "-beta" ]; then
	BETA="${TEXTBOTINFO04}"
else
	BETA="${TEXTBOTINFO05}"
fi

if ! [ "${WEBIFBIND[$BOTC2]}" = "" ]; then
	WEBIFBIND="${WEBIFBIND[$BOTC2]}"
else
	WEBIFBIND="n.a."
fi

if ! [ "${SECRETKEY[$BOTC2]}" = "" ]; then
	SECRETKEY="${SECRETKEY[$BOTC2]}"
else
	SECRETKEY="n.a."
fi

if [ -f notportable ] || [ -f TS3MusicBot_${BOTC2}/notportable ]; then
	NOTPORTABLEINFO="${TEXTBOTINFO06}"
else
	NOTPORTABLEINFO=""
fi

clientstatus

cd TS3MusicBot_${BOTC2}/

if as_user "screen -list" | grep -q -w ${SCREENNAME[$BOTC2]}; then
	if [ "${WEBIFCHECK}" = "1" ]; then
		WEBIFCHECK2=$(wget -T 2 -t 1 --server-response http://${IP}:${PORT[$BOTC2]}/favicon.ico -O - 2>&1| grep -c 'HTTP/1.1 200 OK')
		if [ "${WEBIFCHECK2}" = "1" ]; then
			if [ "$Clientstatus" = "OK" ]; then
				STATUSINFO="[\u2714] Client: [\u2714] ${TEXTSTATUSBOT01} [\u2714] $IP:${PORT[$BOTC2]}"
			else
				STATUSINFO="[\u2714] Client: [X] ${TEXTSTATUSBOT01} [\u2714] $IP:${PORT[$BOTC2]}"
			fi
		else
			if [ "$Clientstatus" = "OK" ]; then
				STATUSINFO="[\u2714] Client: [\u2714] ${TEXTSTATUSBOT01} [X] $IP:${PORT[$BOTC2]}"
			else
				STATUSINFO="[\u2714] Client: [X] ${TEXTSTATUSBOT01} [X] $IP:${PORT[$BOTC2]}"
			fi
		fi
	else
		if [ "$Clientstatus" = "OK" ]; then
			STATUSINFO="[\u2714] Client: [\u2714] ${TEXTSTATUSBOT01} $IP:${PORT[$BOTC2]}"
		else
			STATUSINFO="[\u2714] Client: [X] ${TEXTSTATUSBOT01} $IP:${PORT[$BOTC2]}"
		fi
	fi
elif [ ! -f .run ]; then
	STATUSINFO="[Not started]"
else STATUSINFO="[ERROR] Please check the log File"
fi

cd ${BINARYPATH}

as_user "echo \"${TEXTBOTINFO07} ${PORT[$BOTC2]}\" > textbox"
as_user "echo \"\" >> textbox"
as_user "echo \"${TEXTBOTINFO08} ${EMAIL[$BOTC2]}\" >> textbox"
as_user "echo \"${TEXTBOTINFO09} ${MASTERPW[$BOTC2]}\" >> textbox"
as_user "echo \"${TEXTBOTINFO10} ${USERPW[$BOTC2]}\" >> textbox"
as_user "echo \"${TEXTBOTINFO11} ${PORT[$BOTC2]}\" >> textbox"
as_user "echo \"${TEXTBOTINFO12} ${QUERY}\" >> textbox"
as_user "echo \"${TEXTBOTINFO13} ${BETA}\" >> textbox"
as_user "echo \"${TEXTBOTINFO14} ${MAXDISKSPACE[$BOTC2]} MB\" >> textbox"
as_user "echo \"${TEXTBOTINFO15} ${NUMBER}\" >> textbox"
as_user "echo \"${TEXTBOTINFO16} ${WEBIFBIND}\" >> textbox"
as_user "echo \"${TEXTBOTINFO17} ${SECRETKEY}\" >> textbox"
as_user "echo \"${TEXTBOTINFO18} ${SCREENNAME[$BOTC2]}\" >> textbox"
as_user "echo \"${NOTPORTABLEINFO}\" >> textbox"
as_user "echo \"\" >> textbox"
as_user "printf \"${TEXTBOTINFO19} ${STATUSINFO}\" >> textbox"

whiptail --backtitle "${BACKTITLE}" --title "${TITELBOTINFO}" --textbox textbox 22 78

as_user "rm textbox"

}

extras () {

CHOICE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --menu "${MENU}" --nocancel 22 40 10 \
"1" "${TEXTEXTRAS01}" \
"2" "${TEXTEXTRAS02}" \
"3" "${TEXTEXTRAS03}" \
"4" "${TEXTEXTRAS04}" \
"5" "${TEXTEXTRAS05}" \
"6" "${TEXTEXTRAS06}" \
"7" "${TEXTEXTRAS07}" \
"8" "${TEXTEXTRAS08}" \
"9" "${TEXTEXTRAS09}" \
"10" "${TEXTEXTRAS10}"  3>&1 1>&2 2>&3)

case $CHOICE in
	1)
		extrasauto
		;;
	2)
		REINSTALL="1"
		deletebot
		REINSTALL=""

		extras
		;;
	3)
		languagemenu

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/LANGUAGE=.*/LANGUAGE=\'${LANGUAGE}\'/g\" settings.conf"

		source settings.conf
		extras
		;;
	4)
		useredit
		extras
		;;
	5)
		MUSICANDRADIOFOLDERNOW="${MUSICANDRADIOFOLDER}"
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLESETTINGSUPDATE}" --yesno "${TEXTEXTRASFILES01}" 10 78 --defaultno) then
			MUSICANDRADIOFOLDER="1"
		else
			MUSICANDRADIOFOLDER="0"
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/MUSICANDRADIOFOLDER=.*/MUSICANDRADIOFOLDER=\'${MUSICANDRADIOFOLDER}\'/g\" settings.conf"

		source settings.conf

		if ! [ "${MUSICANDRADIOFOLDER}" = "${MUSICANDRADIOFOLDERNOW}" ];then
			if [ "${MUSICANDRADIOFOLDER}" = "1" ]; then
				BOTC=""
				while ! [ "${BOTC}" = "${BOTS}" ]; do
					(( BOTC += 1 ))
					BOTC2=${BOTA[$BOTC]}

					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio/. ${BINARYPATH}/radio"
					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot_${BOTC2}/music/. ${BINARYPATH}/music"
					as_user "cp -R -u -v ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist/. ${BINARYPATH}/playlist"
					if [ ! ${BINARYPATH}/radio ] || [ ! ${BINARYPATH}/music ] || [ ! ${BINARYPATH}/playlist ]; then
						printf "${RED}ERROR: folder radio, music or playlist not Copied!${MAIN}"
						logg "ERROR" "ERROR: folder radio, music or playlist not Copied!"
						logg "NORMAL" "==== END (Extras Folder) ===="
						exit 1
					fi

					sleep 2

					as_user "rm -R ${BINARYPATH}/TS3MusicBot_${BOTC2}/music"
					as_user "rm -R ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio"
					as_user "rm -R ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist"
					if [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio ] || [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/music ] || [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist ]; then
						printf "${RED}ERROR: folder radio, music or playlist not Deleted!${MAIN}"
						logg "ERROR" "ERROR: folder radio, music or playlist not Deleted!"
					fi
					as_user "ln -s ${BINARYPATH}/music ${BINARYPATH}/TS3MusicBot_${BOTC2}/"
					as_user "ln -s ${BINARYPATH}/radio ${BINARYPATH}/TS3MusicBot_${BOTC2}/"
					as_user "ln -s ${BINARYPATH}/playlist ${BINARYPATH}/TS3MusicBot_${BOTC2}/"
					if [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio ] || [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/music ] || [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist ]; then
						printf "${RED}ERROR: folder radio, music or playlist not Linked!${MAIN}"
						logg "ERROR" "ERROR: folder radio, music or playlist not Linked!"
					fi
				done

				whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASFILES02}" 8 78 3>&1 1>&2 2>&3
			fi

			if [ "${MUSICANDRADIOFOLDER}" = "0" ]; then
				BOTC=""
				while ! [ "${BOTC}" = "${BOTS}" ]; do
					(( BOTC += 1 ))
					BOTC2=${BOTA[$BOTC]}

					as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/music"
					as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio"
					as_user "rm ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist"
					if [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio ] || [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/music ] || [ ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist ]; then
						printf "${RED}ERROR: folder radio, music or playlist not Deleted!${MAIN}"
						logg "ERROR" "ERROR: folder radio, music or playlist not Deleted!"
					fi
					as_user "mkdir ${BINARYPATH}/TS3MusicBot_${BOTC2}/music"
					as_user "mkdir ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio"
					as_user "mkdir ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist"
					if [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/radio ] || [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/music ] || [ ! ${BINARYPATH}/TS3MusicBot_${BOTC2}/playlist ]; then
						printf "${RED}ERROR: folder radio, music or playlist not Created!${MAIN}"
						logg "ERROR" "ERROR: folder radio, music or playlist not Created!"
					fi
				done

				whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASFILES03}" 8 78 3>&1 1>&2 2>&3
			fi

		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASFILES04}" 8 78 3>&1 1>&2 2>&3
		fi

		extras
		;;
	6)
		RESTARTTIME=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --inputbox "${TEXTEXTRASRESTARTTIME01}" 9 78 "${RESTARTTIME}" --nocancel 3>&1 1>&2 2>&3)

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/RESTARTTIME=.*/RESTARTTIME=\'${RESTARTTIME}\'/g\" settings.conf"

		source settings.conf

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASRESTARTTIME02} ${RESTARTTIME} ${TEXTEXTRASRESTARTTIME03}" 8 78 3>&1 1>&2 2>&3

		extras
		;;
	7)
		RADIOLIST=()
		BOTC=0
		while ! [ "${BOTC}" = "${BOTS}" ]; do
			(( BOTC += 1 ))
			BOTC2=${BOTA[$BOTC]}

			RADIOLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")

		done

		let ARLENGTH=${BOTS}
		BOTNUMMER2=$(whiptail --radiolist --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" "${TEXTEXTRASBOTAUSWAHL01}" 22 40 12 "${RADIOLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
		BOTNUMMER3=${BOTA[$BOTNUMMER2]}
		as_user "echo ${BOTNUMMER3} > temp"

		BOTNUMMER=$(sed 's/\"//g' temp)
		as_user "rm temp"

		if [ "${BOTNUMMER}" = "" ]; then
			extras
		fi

		extrasbot
		;;
	8)
		if ! [ "${ME}" = "root" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASLINK01}" 8 78 3>&1 1>&2 2>&3
		else
			if [ "${SHORTCUT}" = "" ]; then
				if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --yesno "${TEXTEXTRASLINK02}" 9 78) then
					ln -s ${BINARYPATH}/${DATEINAME} /usr/local/bin/
					if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
					as_user "sed -i -e \"s/SHORTCUT=.*/SHORTCUT=\'\/usr\/local\/bin\/${DATEINAME}\'/g\" settings.conf"
					source settings.conf
					whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASLINK03}" 8 78 3>&1 1>&2 2>&3
				fi
			else
				if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --yesno "${TEXTEXTRASLINK04}" 9 78) then
					rm -R ${SHORTCUT}
					if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
					as_user "sed -i -e \"s/SHORTCUT=.*/SHORTCUT=\'\'/g\" settings.conf"
					source settings.conf
					whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASLINK05}" 8 78 3>&1 1>&2 2>&3
				fi
			fi
		fi
		extras
		;;
	9)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --yesno "${TEXTEXTRASWEBIF01}" 8 78) then
			WEBIFCHECK="1"
		else
			WEBIFCHECK="0"
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/WEBIFCHECK=.*/WEBIFCHECK=\'${WEBIFCHECK}\'/g\" settings.conf"

		source settings.conf

		if [ "${WEBIFCHECK}" = "1" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASWEBIF02} ${TEXTEXTRASWEBIF03}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASWEBIF02} ${TEXTEXTRASWEBIF04}" 8 78 3>&1 1>&2 2>&3
		fi
		extras
		;;
	10)
		hauptmenu
		;;

esac

}

extrasauto () {

CHOICE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --menu "${MENU}" --nocancel 22 40 10 \
"1" "${TEXTEXTRASAUTO01}" \
"2" "${TEXTEXTRASAUTO02}" \
"3" "${TEXTEXTRASAUTO03}"  3>&1 1>&2 2>&3)

case $CHOICE in
1)
		as_user "echo \"${TEXTEXTRASAUTOSTART01}\" > textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART02}\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART03}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART04}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART05}\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART06}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART07}\" >> textbox"
		as_user "echo \"@reboot ${D1} boot > /dev/null 2>&1\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART08}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTOSTART09}\" >> textbox"
		if [ ! -f textbox ]; then logg "ERROR" "ERROR: textbox not Created!"; fi

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRASAUTOSTART}" --textbox textbox 20 80
		as_user "rm textbox"; if [ -f textbox ]; then logg "ERROR" "ERROR: textbox not Deleted!"; fi
		extrasauto
		;;
	2)
		as_user "echo \"${TEXTEXTRASAUTORESTART01}\" > textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART02}\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART03}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART04}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART05}\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART06}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART07}\" >> textbox"
		as_user "echo \"1 5 * * * ${D1} autorestart > /dev/null 2>&1\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART08}\" >> textbox"
		as_user "echo \"\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART09}\" >> textbox"
		as_user "echo \"${TEXTEXTRASAUTORESTART10}\" >> textbox"
		if [ ! -f textbox ]; then logg "ERROR" "ERROR: textbox not Created!"; fi

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRASAUTORESTART}" --textbox textbox 22 85
		as_user "rm textbox"; if [ -f textbox ]; then logg "ERROR" "ERROR: textbox not Deleted!"; fi
		extrasauto
		;;
	3)
		extras
		;;

esac

}

useredit () {

if [ "${ME}" = "root" ]; then
	USERN=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEUSER}" --inputbox "${TEXTUSEREDIT01}" 8 78 ${USER} 3>&1 1>&2 2>&3)

	if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
	sed -i -e s/USER=.*/USER=\'${USERN}\'/g settings.conf

	chown -R ${USERN}:${USERN} ${BINARYPATH}/

	source settings.conf

	whiptail --backtitle "${BACKTITLE}" --title "${TITLEUSER}" --msgbox "${TEXTUSEREDIT01} ${USER} ${TEXTUSEREDIT02}" 8 78 3>&1 1>&2 2>&3
else
	whiptail --backtitle "${BACKTITLE}" --title "${TITLEUSER}" --msgbox "${TEXTUSEREDIT03}" 8 78 3>&1 1>&2 2>&3
fi

}

extrasbot () {

TITLEEXTRASBOT="${NAME} - ${VERSION} - Bot ${BOTNUMMER}"
CHOICE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRASBOT}" --menu "${MENU}" --nocancel 22 40 10 \
"1" "${TEXTEXTRASBOT01}" \
"2" "${TEXTEXTRASBOT02}" \
"3" "${TEXTEXTRASBOT03}" \
"4" "${TEXTEXTRASBOT04}" \
"5" "${TEXTEXTRASBOT05}" \
"6" "${TEXTEXTRASBOT06}" \
"7" "${TEXTEXTRASBOT07}" \
"8" "${TEXTEXTRASBOT08}" \
"9" "${TEXTEXTRASBOT09}" \
"10" "${TEXTEXTRASBOT10}"  3>&1 1>&2 2>&3)

case $CHOICE in
	1)
		EMAIL[${BOTNUMMER}]=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --inputbox "${TEXTEXTRASBOTEMAIL01}" 8 78 ${EMAIL[$BOTNUMMER]} 3>&1 1>&2 2>&3)

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/EMAIL\[${BOTNUMMER}\]=.*/EMAIL[${BOTNUMMER}]=\'${EMAIL[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTEMAIL01} ${EMAIL[$BOTNUMMER]} ${TEXTEXTRASBOTEMAIL02}" 8 78 3>&1 1>&2 2>&3

		extrasbot
		;;
	2)
		extrasbotpasswort
		;;
	3)
		portchange

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/# Bot ${BOTNUMMER}/# Bot ${PORTN[$BOTNUMMER]}/g\" settings.conf"
		as_user "sed -i -e \"s/PORT\[${BOTNUMMER}\]=.*/PORT[${PORTN[$BOTNUMMER]}]=\'${PORTN[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/EMAIL\[${BOTNUMMER}\]=.*/EMAIL[${PORTN[$BOTNUMMER]}]=\'${EMAIL[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/MASTERPW\[${BOTNUMMER}\]=.*/MASTERPW[${PORTN[$BOTNUMMER]}]=\'${MASTERPW[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/USERPW\[${BOTNUMMER}\]=.*/USERPW[${PORTN[$BOTNUMMER]}]=\'${USERPW[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/QUERY\[${BOTNUMMER}\]=.*/QUERY[${PORTN[$BOTNUMMER]}]=\'${QUERY[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/MAXDISKSPACE\[${BOTNUMMER}\]=.*/MAXDISKSPACE[${PORTN[$BOTNUMMER]}]=\'${MAXDISKSPACE[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/NUMBER\[${BOTNUMMER}\]=.*/NUMBER[${PORTN[$BOTNUMMER]}]=\'${NUMBER[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/BETA\[${BOTNUMMER}\]=.*/BETA[${PORTN[$BOTNUMMER]}]=\'${BETA[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/WEBIFBIND\[${BOTNUMMER}\]=.*/WEBIFBIND[${PORTN[$BOTNUMMER]}]=\'${WEBIFBIND[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/SECRETKEY\[${BOTNUMMER}\]=.*/SECRETKEY[${PORTN[$BOTNUMMER]}]=\'${SECRETKEY[$BOTNUMMER]}\'/g\" settings.conf"
		as_user "sed -i -e \"s/SCREENNAME\[${BOTNUMMER}\]=.*/SCREENNAME[${PORTN[$BOTNUMMER]}]=\'${SCREENNAME[$BOTNUMMER]}\'/g\" settings.conf"

		if [ ! -w bots.ini ]; then logg "ERROR" "ERROR: bots.ini not exist or not writable!"; fi
		as_user "sed -i -e \"s/${BOTNUMMER}/${PORTN[$BOTNUMMER]}/g\" bots.ini"
		as_user "sort --numeric-sort bots.ini -o bots.ini"

		as_user "mv ${BINARYPATH}/TS3MusicBot_${BOTNUMMER} ${BINARYPATH}/TS3MusicBot_${PORTN[BOTNUMMER]}"
		if [ ! -d ${BINARYPATH}/TS3MusicBot_${PORTN[BOTNUMMER]} ]; then logg "ERROR" "ERROR: Bot folder Not Moved!"; fi

		source settings.conf

		BOTA=($(< bots.ini ))
		BOTS=${#BOTA[@]}
		(( BOTS -=1 ))

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTPORT01} ${PORTN[$BOTNUMMER]} ${TEXTEXTRASBOTPORT02}" 8 78 3>&1 1>&2 2>&3

		BOTNUMMER=${PORTN[$BOTNUMMER]}

		extrasbot
		;;
	4)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --yesno --defaultno "${TEXTEXTRASBOTQUERY01}" 8 78) then
			QUERY[${BOTNUMMER}]="-query"
		else
			QUERY[${BOTNUMMER}]="-noquery"
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/QUERY\[${BOTNUMMER}\]=.*/QUERY[${BOTNUMMER}]=\'${QUERY[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if [ "${QUERY[$BOTNUMMER]}" = "-query" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTQUERY02}" 8 78 3>&1 1>&2 2>&3
		elif [ "${QUERY[$BOTNUMMER]}" = "-noquery" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTQUERY03}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	5)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --yesno "${TEXTEXTRASBOTDISKSPACE04}" 8 78 --defaultno) then
			if [ "${MAXDISKSPACE[$BOTNUMMER]}" = "-1" ];then
				MAXDISKSPACE[${BOTNUMMER}]='10240'
			fi
			MAXDISKSPACE[${BOTNUMMER}]=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --inputbox "${TEXTEXTRASBOTDISKSPACE01}" 8 78 "${MAXDISKSPACE[$BOTNUMMER]}" 3>&1 1>&2 2>&3)
		else
			MAXDISKSPACE[${BOTNUMMER}]="-1"
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/MAXDISKSPACE\[${BOTNUMMER}\]=.*/MAXDISKSPACE[${BOTNUMMER}]=\'${MAXDISKSPACE[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if [ "${MAXDISKSPACE[$BOTNUMMER]}" = "-1" ];then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTDISKSPACE05}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTDISKSPACE02} ${MAXDISKSPACE[$BOTNUMMER]} ${TEXTEXTRASBOTDISKSPACE03}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	6)
		if [ "${NUMBER[$BOTNUMMER]}" = "-number 1" ]; then
			CONFIG1="ON"
		else
			CONFIG1="OFF"
		fi

		if [ "${NUMBER[$BOTNUMMER]}" = "-number 2" ]; then
			CONFIG2="ON"
		else
			CONFIG2="OFF"
		fi

		if [ "${NUMBER[$BOTNUMMER]}" = "-number 3" ]; then
			CONFIG3="ON"
		else
			CONFIG3="OFF"
		fi
		
		if [ "${NUMBER[$BOTNUMMER]}" = "-number 4" ]; then
			CONFIG4="ON"
		else
			CONFIG4="OFF"
		fi
		
		if [ "${NUMBER[$BOTNUMMER]}" = "-number 5" ]; then
			CONFIG5="ON"
		else
			CONFIG5="OFF"
		fi
		
		if [ "${NUMBER[$BOTNUMMER]}" = "-number 6" ]; then
			CONFIG6="ON"
		else
			CONFIG6="OFF"
		fi

		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --yesno --defaultno "${TEXTEXTRASBOTCONFIG01}" 8 78) then
			NUMBER[$BOTNUMMER]="-"$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --nocancel --radiolist \
			"${TEXTEXTRASBOTCONFIG02}" 15 48 6 \
			"number 1" "${TEXTEXTRASBOTCONFIG03}" ${CONFIG1} \
			"number 2" "${TEXTEXTRASBOTCONFIG04}" ${CONFIG2} \
			"number 3" "${TEXTEXTRASBOTCONFIG05}" ${CONFIG3} \
			"number 4" "${TEXTEXTRASBOTCONFIG09}" ${CONFIG4} \
			"number 5" "${TEXTEXTRASBOTCONFIG10}" ${CONFIG5} \
			"number 6" "${TEXTEXTRASBOTCONFIG11}" ${CONFIG6} \
			3>&1 1>&2 2>&3)
		else
			NUMBER[${BOTNUMMER}]=""
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/NUMBER\[${BOTNUMMER}\]=.*/NUMBER[${BOTNUMMER}]=\'${NUMBER[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if ! [ "${NUMBER[$BOTNUMMER]}" = "" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTCONFIG06} ${NUMBER[$BOTNUMMER]} ${TEXTEXTRASBOTCONFIG07}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTCONFIG08}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	7)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --yesno --defaultno "${TEXTEXTRASBOTBETA01}" 10 78) then
			BETA[${BOTNUMMER}]="-beta"
		else
			BETA[${BOTNUMMER}]=""
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/BETA\[${BOTNUMMER}\]=.*/BETA[${BOTNUMMER}]=\'${BETA[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if [ "${BETA[$BOTNUMMER]}" = "-beta" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTBETA02}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTBETA03}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	8)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTEXTRASBOTBINDIP01}" 8 78 --defaultno) then
			WEBIFBIND=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTEXTRASBOTBINDIP02}" 8 78 ${WEBIFBIND[$BOTNUMMER]} --nocancel 3>&1 1>&2 2>&3)
			WEBIFBIND[${BOTNUMMER}]=${WEBIFBIND}
		else
			WEBIFBIND[${BOTNUMMER}]=""
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/WEBIFBIND\[${BOTNUMMER}\]=.*/WEBIFBIND[${BOTNUMMER}]=\'${WEBIFBIND[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if ! [ "${WEBIFBIND[$BOTNUMMER]}" = "" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTBINDIP03} ${WEBIFBIND} ${TEXTEXTRASBOTBINDIP04}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTBINDIP05}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	9)
		if (whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --yesno "${TEXTEXTRASBOTSECRETKEY01}" 8 78 --defaultno) then
			SECRETKEY=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --inputbox "${TEXTEXTRASBOTSECRETKEY02}" 8 78 ${SECRETKEY[$BOTNUMMER]} --nocancel 3>&1 1>&2 2>&3)
			SECRETKEY[${BOTNUMMER}]="${SECRETKEY}"
		else
			SECRETKEY[${BOTNUMMER}]=""
		fi

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/SECRETKEY\[${BOTNUMMER}\]=.*/SECRETKEY[${BOTNUMMER}]=\'${SECRETKEY[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		if ! [ "${SECRETKEY[$BOTNUMMER]}" = "" ]; then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTSECRETKEY03} ${SECRETKEY} ${TEXTEXTRASBOTSECRETKEY04}" 8 78 3>&1 1>&2 2>&3
		else
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTSECRETKEY05}" 8 78 3>&1 1>&2 2>&3
		fi

		extrasbot
		;;
	10)
		extras
		;;

esac

}

portchange () {

PORTN=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --inputbox "${TEXTEXTRASBOTPORT01}" --nocancel 8 78 ${PORT[$BOTNUMMER]} 3>&1 1>&2 2>&3)

if [ "${PORTN}" -le "1023" ];then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTEXTRASBOTPORT03}" 10 78
	portchange
elif [ "${PORTN}" -gt "65535" ]; then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTEXTRASBOTPORT03}" 10 78
	portchange
elif [ "${PORTN}" = "" ];then
	whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTEXTRASBOTPORT04}" 10 78
	portchange
elif [ "${PORTN}" = "${PORT[$BOTNUMMER]}" ];then
	PORTN[${BOTNUMMER}]="${PORTN}"
	return
else
	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}
		if [ "${PORT[$BOTC2]}" = "${PORTN}" ];then
			whiptail --backtitle "${BACKTITLE}" --title "${TITLECREATEBOT}" --msgbox "${TEXTEXTRASBOTPORT05}" 10 78
			portchange
		fi
	done
fi

PORTN[${BOTNUMMER}]="${PORTN}"

}

extrasbotpasswort () {

CHOICE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRASBOT}" --menu "${MENU}" --nocancel 22 40 3 \
"1" "${TEXTEXTRASBOTPASSWORD01}" \
"2" "${TEXTEXTRASBOTPASSWORD02}" \
"3" "${TEXTEXTRASBOTPASSWORD03}"  3>&1 1>&2 2>&3)

case $CHOICE in
	1)
		MASTERPW[${BOTNUMMER}]=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --inputbox "${TEXTEXTRASBOTPASSWORD04}" 8 78 ${MASTERPW[$BOTNUMMER]} --nocancel 3>&1 1>&2 2>&3)

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/MASTERPW\[${BOTNUMMER}\]=.*/MASTERPW[${BOTNUMMER}]=\'${MASTERPW[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTPASSWORD04} ${MASTERPW[$BOTNUMMER]} ${TEXTEXTRASBOTPASSWORD05}" 8 78

		extrasbot
		;;
	2)
		USERPW[${BOTNUMMER}]=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --inputbox "${TEXTEXTRASBOTPASSWORD06}" 8 78 ${USERPW[$BOTNUMMER]} --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --nocancel 3>&1 1>&2 2>&3)

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i -e \"s/USERPW\[${BOTNUMMER}\]=.*/USERPW[${BOTNUMMER}]=\'${USERPW[$BOTNUMMER]}\'/g\" settings.conf"

		source settings.conf

		whiptail --backtitle "${BACKTITLE}" --title "${TITLEBOTDATENAENDERN}" --msgbox "${TEXTEXTRASBOTPASSWORD06} ${USERPW[$BOTNUMMER]} ${TEXTEXTRASBOTPASSWORD05}" 8 78

		extrasbot
		;;
	3)
		extrasbot
		;;
esac

}

deletebot () {

if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEDELETEBOT}" --yesno "${TEXTDELETEBOT01}" 10 78 --yes-button "${TEXTDELETEBOT02}" --no-button "${TEXTDELETEBOT03}") then
	return
else
	CHECKLIST=()
	BOTC=0
	while ! [ "${BOTC}" = "${BOTS}" ]; do
		(( BOTC += 1 ))
		BOTC2=${BOTA[$BOTC]}

		CHECKLIST+=("${BOTC}" "TS3MusicBot ${BOTC2}" "OFF")

	done

	let ARLENGTH=${BOTS}
	BOTSTART=$(whiptail --checklist --backtitle "${BACKTITLE}" --title "${TITLEDELETEBOT}" "${TEXTDELETEBOT04}" 20 40 12 "${CHECKLIST[@]}" --nocancel 3>&1 1>&2 2>&3)
	as_user "echo ${BOTSTART} > temp"

	BOTSTART2=$(sed 's/\"//g' temp)
	BOTSTART3=( ${BOTSTART2} )
	as_user "rm temp"

	clear
	BOTC3=0

	while ! [ "${BOTC3}" = "${#BOTSTART3[@]}" ]; do
		BOTC=${BOTSTART3[$BOTC3]}
		(( BOTC3 += 1 ))
		BOTC2=${BOTA[$BOTC]}

		deletebot2
	done

	botsinicheck

fi

}

deletebot2 () {

if (whiptail --backtitle "${BACKTITLE}" --title "${TITLEDELETEBOT}" --fb --yesno "${TEXTDELETEBOT05} TS3MusicBot_${BOTC2} ${TEXTDELETEBOT06}" 12 78 --yes-button "${TEXTDELETEBOT02}" --no-button "${TEXTDELETEBOT03}") then
	return
else
	as_user "rm -R ${BINARYPATH}/TS3MusicBot_${BOTC2}"
	if [ -d ${BINARYPATH}/TS3MusicBot_${BOTC2} ]; then logg "ERROR" "ERROR: Bot folder Not Deleted!"; fi

	if [ -f ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log ]; then
		logg "" " Screen Log File Exist. Delete it"
		as_user "rm ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log 2> /dev/null"
		if [ ! -f ${BINARYPATH}/logs/screen_${SCREENNAME[$BOTC2]}.log ]; then logg "" "  Removed Successfully"; else logg "ERROR" "  ERROR: File not Deleted!"; fi
	fi

	if [ "$REINSTALL" = "1" ]; then
		logg "" "Delete Bot ${BOTC2}"
		downloadbot

		if [ -d ${BINARYPATH}/TS3MusicBot_${BOTC2} ]; then
			logg "" " Bot successfully reinstalled"
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASREINSTALL01}" 8 78 3>&1 1>&2 2>&3
		else
			logg "ERROR" "ERROR: Bot not reinstalled!"
			whiptail --backtitle "${BACKTITLE}" --title "${TITLEEXTRAS}" --msgbox "${TEXTEXTRASREINSTALL02}" 8 78 3>&1 1>&2 2>&3
		fi
	else
		if [ ! -w bots.ini ]; then logg "ERROR" "ERROR: bots.ini not exist or not writable!"; fi
		as_user "sed -i '/^${BOTC2}/d' bots.ini"

		if [ ! -w settings.conf ]; then logg "ERROR" "ERROR: settings.conf not exist or not writable!"; fi
		as_user "sed -i '/^# Bot ${BOTC2}/d' settings.conf"
		as_user "sed -i '/^PORT\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^EMAIL\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^MASTERPW\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^USERPW\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^QUERY\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^MAXDISKSPACE\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^NUMBER\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^BETA\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^WEBIFBIND\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^SECRETKEY\[${BOTC2}\]/d' settings.conf"
		as_user "sed -i '/^SCREENNAME\[${BOTC2}\]/d' settings.conf"

		source settings.conf

		logg "" "Delete Bot ${BOTC2}"
	fi

fi

}

# Hauptmenu des Sciptes
hauptmenu () {

CHOICE=$(whiptail --backtitle "${BACKTITLE}" --title "${TITLEHAUPTMENU}" --menu "${MENU}" --nocancel 22 40 10 \
"1" "${TEXTHAUPTMENU01}" \
"2" "${TEXTHAUPTMENU02}" \
"3" "${TEXTHAUPTMENU03}" \
"4" "${TEXTHAUPTMENU04}" \
"5" "${TEXTHAUPTMENU05}" \
"6" "${TEXTHAUPTMENU06}" \
"7" "${TEXTHAUPTMENU07}" \
"8" "${TEXTHAUPTMENU08}" \
"9" "${TEXTHAUPTMENU09}" \
"10" "${TEXTHAUPTMENU10}"  3>&1 1>&2 2>&3)

case $CHOICE in
	1)
		statusbot
		;;
	2)
		startbotauswahl
		;;
	3)
		stopbotauswahl
		;;
	4)
		restartbotauswahl
		;;
	5)
		joinscreen
		hauptmenu
		;;
	6)
		createbot
		hauptmenu
		;;
	7)
		botinfo
		hauptmenu
		;;
	8)
		deletebot
		hauptmenu
		;;
	9)
		extras
		;;
	10)
		logg "NORMAL" "==== END (Graphical Menu) ===="
		exit 0
		;;
esac

}

# Startparameter
case "$1" in
	start)
		startbot $*
	;;
	stop)
		stopbot $*
	;;
	restart)
		restartbot $*
	;;
	status)
		statusbot
	;;
	boot)
		sleep $BOOTTIME
		export TERM=${TERM:-dumb}
		BOOTPRINT="off"
		startbot $*
	;;
	autorestart)
		export TERM=${TERM:-dumb}
		BOOTPRINT="off"
		restartbot $*
	;;
	help)
		help
	;;
	update)
		vcheck
	;;
	menu)
		hauptmenu
	;;
	"")
		hauptmenu
	;;
	*)
		echo "Incorrect input! Use: ${0} help"
		logg "NORMAL" "==== END (Incorrect input) ===="
		exit 1
	;;
esac

logg "NORMAL" "==== END (Script) ===="

exit 0